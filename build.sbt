name := """cinema-management-system"""
organization := "dan301290.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.12.2"

libraryDependencies += guice
libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1201-jdbc41"
libraryDependencies += "com.h2database" % "h2" % "1.4.192"
libraryDependencies += javaJdbc
libraryDependencies += evolutions

//resolvers += "atlassian internal" at "https://maven.atlassian.com/repository/internal/"
//libraryDependencies += "me.shib.java.lib" % "omdb-java" % "1.2.0"

dependencyOverrides ++= Set(
  "com.typesafe.play" % "play-jdbc_2.12" % "2.6.6",
  "com.typesafe.play" % "play-jdbc-api_2.12" % "2.6.6",
  "com.typesafe.play" % "play-jdbc-evolutions_2.12" % "2.6.6",
  "com.typesafe.play" % "play-java-jdbc_2.12" % "2.6.6"
)