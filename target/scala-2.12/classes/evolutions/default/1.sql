# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table booking (
  booking_id                    integer auto_increment not null,
  showing_id                    varchar(255),
  customer_id                   varchar(255),
  seat_num                      varchar(255),
  date                          varchar(255),
  constraint pk_booking primary key (booking_id)
);

create table customer (
  customer_id                   integer auto_increment not null,
  first_name                    varchar(255),
  last_name                     varchar(255),
  email                         varchar(255),
  phone                         varchar(255),
  address                       varchar(255),
  constraint pk_customer primary key (customer_id)
);

create table movie (
  movie_id                      integer auto_increment not null,
  movie_name                    varchar(255),
  age_rating                    varchar(255),
  short_desc                    varchar(255),
  plot                          varchar(255),
  img_url                       varchar(255),
  year                          varchar(255),
  genre                         varchar(255),
  released                      varchar(255),
  director                      varchar(255),
  runtime                       varchar(255),
  constraint pk_movie primary key (movie_id)
);

create table screen (
  screen_id                     integer auto_increment not null,
  total_seats                   varchar(255),
  screen_name                   varchar(255),
  constraint pk_screen primary key (screen_id)
);

create table showing (
  showing_id                    integer auto_increment not null,
  screen_id                     varchar(255),
  start_time                    varchar(255),
  end_time                      varchar(255),
  date                          varchar(255),
  constraint pk_showing primary key (showing_id)
);

create table user (
  user_id                       integer auto_increment not null,
  user_name                     varchar(255),
  password                      varchar(255),
  constraint pk_user primary key (user_id)
);


# --- !Downs

drop table if exists booking;

drop table if exists customer;

drop table if exists movie;

drop table if exists screen;

drop table if exists showing;

drop table if exists user;

