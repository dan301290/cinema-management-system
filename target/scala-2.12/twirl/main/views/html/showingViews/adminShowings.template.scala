
package views.html.showingViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.Showing
/*3.2*/import java.util

object adminShowings extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[util.List[Showing],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(showings: util.List[Showing]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.32*/("""

"""),_display_(/*6.2*/adminMain("Cinema-Admin-Showings")/*6.36*/ {_display_(Seq[Any](format.raw/*6.38*/("""
        """),format.raw/*7.9*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Dashboard - Cinema Manager</a>
            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Settings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Help</a>
                    </li>
                </ul>
                <form class="form-inline mt-2 mt-md-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                    <ul class="nav nav-pills flex-column">
                        <li class="nav-item">
                            <a class="nav-link" href="/admin">Overview</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/movies">Movies</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="/admin/showings">Showings <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/bookings">Bookings</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/customers">Customers</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/screens">Screens</a>
                        </li>
                    </ul>
                </nav>

                <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
                    <h1>Dashboard - Showings</h1>

                    <section class="row text-center placeholders">
                        <div class="col-6 col-sm-3 placeholder">
                            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
                            <h4>Label</h4>
                            <div class="text-muted">current showing capaicty</div>
                        </div>
                        <div class="col-6 col-sm-3 placeholder">
                            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
                            <h4>Label</h4>
                            <span class="text-muted">current seats filled</span>
                        </div>
                        <div class="col-6 col-sm-3 placeholder">
                            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
                            <h4>Label</h4>
                            <span class="text-muted">Something else</span>
                        </div>
                        <div class="col-6 col-sm-3 placeholder">
                            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
                            <h4>Label</h4>
                            <span class="text-muted">Something else</span>
                        </div>
                    </section>

                    <h2>latest Showings</h2>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>showingId</th>
                                    <th>screenId</th>
                                    <th>startTime</th>
                                    <th>endTime</th>
                                    <th>Date</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            """),_display_(/*100.30*/for(showing <- showings) yield /*100.54*/{_display_(Seq[Any](format.raw/*100.55*/("""
                                """),format.raw/*101.33*/("""<tr>
                                    <td>"""),_display_(/*102.42*/showing/*102.49*/.getShowingId),format.raw/*102.62*/("""</td>
                                    <td>"""),_display_(/*103.42*/showing/*103.49*/.getScreenId),format.raw/*103.61*/("""</td>
                                    <td>"""),_display_(/*104.42*/showing/*104.49*/.getStartTime),format.raw/*104.62*/("""</td>
                                    <td>"""),_display_(/*105.42*/showing/*105.49*/.getEndTime),format.raw/*105.60*/("""</td>
                                    <td>"""),_display_(/*106.42*/showing/*106.49*/.getDate),format.raw/*106.57*/("""</td>
                                    <th><a class="btn btn-primary" href="showings/edit/"""),_display_(/*107.89*/showing/*107.96*/.getShowingId),format.raw/*107.109*/("""" role="button">Edit</a></th>
                                    <th><a class="btn btn-primary" href="showings/destroy/"""),_display_(/*108.92*/showing/*108.99*/.getShowingId),format.raw/*108.112*/("""" role="button">Delete</a></th>
                                </tr>
                            """)))}),format.raw/*110.30*/("""
                            """),format.raw/*111.29*/("""</tbody>
                        </table>
                    </div>

                    <a class="btn btn-primary" href="showings/create" role="button">Add New Showing</a>

                </main>
            </div>
        </div>
""")))}))
      }
    }
  }

  def render(showings:util.List[Showing]): play.twirl.api.HtmlFormat.Appendable = apply(showings)

  def f:((util.List[Showing]) => play.twirl.api.HtmlFormat.Appendable) = (showings) => apply(showings)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:07:00 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/showingViews/adminShowings.scala.html
                  HASH: 286a95a2c5e8ab57834e275cd31cd5e41059db1f
                  MATRIX: 664->23|693->47|1034->66|1159->96|1189->101|1231->135|1270->137|1306->147|6793->5606|6834->5630|6874->5631|6937->5665|7012->5712|7029->5719|7064->5732|7140->5780|7157->5787|7191->5799|7267->5847|7284->5854|7319->5867|7395->5915|7412->5922|7445->5933|7521->5981|7538->5988|7568->5996|7691->6091|7708->6098|7744->6111|7894->6233|7911->6240|7947->6253|8080->6354|8139->6384
                  LINES: 24->2|25->3|30->4|35->4|37->6|37->6|37->6|38->7|131->100|131->100|131->100|132->101|133->102|133->102|133->102|134->103|134->103|134->103|135->104|135->104|135->104|136->105|136->105|136->105|137->106|137->106|137->106|138->107|138->107|138->107|139->108|139->108|139->108|141->110|142->111
                  -- GENERATED --
              */
          