
package views.html.showingViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.Showing
/*3.2*/import views.html.helper

object adminShowingEdit extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Showing],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(form: Form[Showing]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.23*/("""

"""),_display_(/*6.2*/adminMain("Cinema-Admin-Showings")/*6.36*/ {_display_(Seq[Any](format.raw/*6.38*/("""
    """),format.raw/*7.5*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Dashboard - Cinema Manager</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Settings</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Help</a>
                </li>
            </ul>
            <form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin">Overview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/movies">Movies</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/admin/showings">Showings <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/bookings">Bookings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/customers">Customers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/screens">Screens</a>
                    </li>
                </ul>
            </nav>

            <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
                <h1>Dashboard - Add Showing</h1>

                <form class="form-AddBooking" action="/admin/showings/update" method="GET">
                    <section class="row placeholders">
                        <div class="col-6 col-sm-6 placeholder">

                            <div class="form-group">
                                <label for="showingId">Showing Id</label>
                                """),_display_(/*69.34*/defining(form("showingId"))/*69.61*/ { uidField =>_display_(Seq[Any](format.raw/*69.75*/("""
                                    """),format.raw/*70.37*/("""<input readonly  type="number" class="form-control" id="showingId" name="showingId" aria-describedby="showingIdHelp" value=""""),_display_(/*70.162*/uidField/*70.170*/.value),format.raw/*70.176*/("""" required autofocus>
                                """)))}),format.raw/*71.34*/("""
                                    """),format.raw/*72.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="screenId">Screen Id</label>
                                """),_display_(/*77.34*/defining(form("screenId"))/*77.60*/ { uidField =>_display_(Seq[Any](format.raw/*77.74*/("""
                                    """),format.raw/*78.37*/("""<input type="number" class="form-control" id="screenId" name="screenId" aria-describedby="screenIdHelp" value=""""),_display_(/*78.149*/uidField/*78.157*/.value),format.raw/*78.163*/("""" placeholder="screenId" required>
                                """)))}),format.raw/*79.34*/("""
                                    """),format.raw/*80.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="startTime">Film Start Time</label>
                                """),_display_(/*85.34*/defining(form("startTime"))/*85.61*/ { uidField =>_display_(Seq[Any](format.raw/*85.75*/("""
                                    """),format.raw/*86.37*/("""<input type="time" class="form-control" id="startTime" name="startTime" aria-describedby="startTimeHelp" value=""""),_display_(/*86.150*/uidField/*86.158*/.value),format.raw/*86.164*/("""" placeholder="startTime" required>
                                """)))}),format.raw/*87.34*/("""
                                    """),format.raw/*88.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="endTime">Film End Time</label>
                                """),_display_(/*93.34*/defining(form("endTime"))/*93.59*/ { uidField =>_display_(Seq[Any](format.raw/*93.73*/("""
                                    """),format.raw/*94.37*/("""<input type="time" class="form-control" id="endTime" name="endTime" aria-describedby="endTimeHelp" value=""""),_display_(/*94.144*/uidField/*94.152*/.value),format.raw/*94.158*/("""" placeholder="endTime" required>
                                """)))}),format.raw/*95.34*/("""
                                    """),format.raw/*96.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="date">Showing Date</label>
                                """),_display_(/*101.34*/defining(form("date"))/*101.56*/ { uidField =>_display_(Seq[Any](format.raw/*101.70*/("""
                                    """),format.raw/*102.37*/("""<input type="date" class="form-control" id="date" name="date" aria-describedby="dateHelp" value=""""),_display_(/*102.135*/uidField/*102.143*/.value),format.raw/*102.149*/("""" placeholder="date" required>
                                """)))}),format.raw/*103.34*/("""
                                    """),format.raw/*104.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="col-xs-10 col-sm-4 col-md-4 col-lg-4">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Update Showing</button>
                            </div>

                        </div>
                    </section>
                </form>

            </main>
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(form:Form[Showing]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Showing]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:07:00 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/showingViews/adminShowingEdit.scala.html
                  HASH: 5545a371aed183936d0f88370c10ea365368beb5
                  MATRIX: 664->23|693->47|1040->74|1156->95|1186->100|1228->134|1267->136|1299->142|4435->3251|4471->3278|4523->3292|4589->3330|4742->3455|4760->3463|4788->3469|4875->3525|4941->3563|5285->3880|5320->3906|5372->3920|5438->3958|5578->4070|5596->4078|5624->4084|5724->4153|5790->4191|6141->4515|6177->4542|6229->4556|6295->4594|6436->4707|6454->4715|6482->4721|6583->4791|6649->4829|6996->5149|7030->5174|7082->5188|7148->5226|7283->5333|7301->5341|7329->5347|7428->5415|7494->5453|7838->5769|7870->5791|7923->5805|7990->5843|8117->5941|8136->5949|8165->5955|8262->6020|8329->6058
                  LINES: 24->2|25->3|30->4|35->4|37->6|37->6|37->6|38->7|100->69|100->69|100->69|101->70|101->70|101->70|101->70|102->71|103->72|108->77|108->77|108->77|109->78|109->78|109->78|109->78|110->79|111->80|116->85|116->85|116->85|117->86|117->86|117->86|117->86|118->87|119->88|124->93|124->93|124->93|125->94|125->94|125->94|125->94|126->95|127->96|132->101|132->101|132->101|133->102|133->102|133->102|133->102|134->103|135->104
                  -- GENERATED --
              */
          