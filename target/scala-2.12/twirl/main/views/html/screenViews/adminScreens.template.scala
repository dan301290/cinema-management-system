
package views.html.screenViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.Screen
/*3.2*/import java.util

object adminScreens extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[util.List[Screen],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(screens: util.List[Screen]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.30*/("""

"""),_display_(/*6.2*/adminMain("Cinema-Admin-Screens")/*6.35*/ {_display_(Seq[Any](format.raw/*6.37*/("""
  """),format.raw/*7.3*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Dashboard - Cinema Manager</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Help</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link" href="/admin">Overview</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/movies">Movies</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/showings">Showings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/bookings">Bookings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/customers">Customers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="/admin/screens">Screens <span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </nav>

      <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
        <h1>Dashboard - Screens</h1>

        <section class="row text-center placeholders">
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <div class="text-muted">current showing capaicty</div>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">current seats filled</span>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">Something else</span>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">Something else</span>
          </div>
        </section>

        <h2>Screens</h2>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>screenId</th>
                <th>totalSeats</th>
                <th>screenName</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
            """),_display_(/*99.14*/for(screen <- screens) yield /*99.36*/{_display_(Seq[Any](format.raw/*99.37*/("""
              """),format.raw/*100.15*/("""<tr>
                <td>"""),_display_(/*101.22*/screen/*101.28*/.getScreenId),format.raw/*101.40*/("""</td>
                <td>"""),_display_(/*102.22*/screen/*102.28*/.getTotalSeats),format.raw/*102.42*/("""</td>
                <td>"""),_display_(/*103.22*/screen/*103.28*/.getScreenName),format.raw/*103.42*/("""</td>
                <th><a class="btn btn-primary" href="screens/edit/"""),_display_(/*104.68*/screen/*104.74*/.getScreenId),format.raw/*104.86*/("""" role="button">Edit</a></th>
                <th><a class="btn btn-primary" href="screens/destroy/"""),_display_(/*105.71*/screen/*105.77*/.getScreenId),format.raw/*105.89*/("""" role="button">Delete</a></th>
              </tr>
            """)))}),format.raw/*107.14*/("""
            """),format.raw/*108.13*/("""</tbody>
          </table>
        </div>

          <a class="btn btn-primary" href="screens/create" role="button">Add New Screen</a>

      </main>
    </div>
  </div>
""")))}))
      }
    }
  }

  def render(screens:util.List[Screen]): play.twirl.api.HtmlFormat.Appendable = apply(screens)

  def f:((util.List[Screen]) => play.twirl.api.HtmlFormat.Appendable) = (screens) => apply(screens)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:07:00 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/screenViews/adminScreens.scala.html
                  HASH: f0891c0d2f0acce8a98932f2825aed0cd5d5a5de
                  MATRIX: 663->23|691->46|1030->65|1153->93|1183->98|1224->131|1263->133|1293->137|5522->4339|5560->4361|5599->4362|5644->4378|5699->4405|5715->4411|5749->4423|5805->4451|5821->4457|5857->4471|5913->4499|5929->4505|5965->4519|6067->4593|6083->4599|6117->4611|6246->4712|6262->4718|6296->4730|6395->4797|6438->4811
                  LINES: 24->2|25->3|30->4|35->4|37->6|37->6|37->6|38->7|130->99|130->99|130->99|131->100|132->101|132->101|132->101|133->102|133->102|133->102|134->103|134->103|134->103|135->104|135->104|135->104|136->105|136->105|136->105|138->107|139->108
                  -- GENERATED --
              */
          