
package views.html.screenViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.Screen
/*3.2*/import views.html.helper

object adminScreenEdit extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Screen],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(form: Form[Screen]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.22*/("""

"""),_display_(/*6.2*/adminMain("Cinema-Admin-Screens")/*6.35*/ {_display_(Seq[Any](format.raw/*6.37*/("""
  """),format.raw/*7.3*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Dashboard - Cinema Manager</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Help</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link" href="/admin">Overview</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/movies">Movies</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/showings">Showings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/bookings">Bookings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/customers">Customers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="/admin/screens">Screens <span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </nav>

      <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
        <h1>Dashboard - Edit Screen</h1>

        <form class="form-AddBooking" action="/admin/screens/update" method="GET">
          <section class="row placeholders">
            <div class="col-6 col-sm-6 placeholder">

              <div class="form-group">
                <label for="screenId">screenId</label>
                """),_display_(/*69.18*/defining(form("screenId"))/*69.44*/ { uidField =>_display_(Seq[Any](format.raw/*69.58*/("""
                  """),format.raw/*70.19*/("""<input readonly  type="text" class="form-control" id="screenId" name="screenId" aria-describedby="screenIdHelp" value=""""),_display_(/*70.139*/uidField/*70.147*/.value),format.raw/*70.153*/("""" required autofocus>
                """)))}),format.raw/*71.18*/("""
                  """),format.raw/*72.19*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
              </div>

              <div class="form-group">
                <label for="totalSeats">totalSeats</label>
                """),_display_(/*77.18*/defining(form("totalSeats"))/*77.46*/ { uidField =>_display_(Seq[Any](format.raw/*77.60*/("""
                  """),format.raw/*78.19*/("""<input type="number" class="form-control" id="totalSeats" name="totalSeats" aria-describedby="totalSeatsHelp" value=""""),_display_(/*78.137*/uidField/*78.145*/.value),format.raw/*78.151*/("""" placeholder="totalSeats" required>
                """)))}),format.raw/*79.18*/("""
                  """),format.raw/*80.19*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
              </div>

              <div class="form-group">
                <label for="screenName">screenName</label>
                """),_display_(/*85.18*/defining(form("screenName"))/*85.46*/ { uidField =>_display_(Seq[Any](format.raw/*85.60*/("""
                  """),format.raw/*86.19*/("""<input type="text" class="form-control" id="screenName" name="screenName" aria-describedby="screenNameHelp" value=""""),_display_(/*86.135*/uidField/*86.143*/.value),format.raw/*86.149*/("""" placeholder="screenName" required>
                """)))}),format.raw/*87.18*/("""
                  """),format.raw/*88.19*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
              </div>

              <div class="col-xs-10 col-sm-4 col-md-4 col-lg-4">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Update Screen</button>
              </div>

            </div>
          </section>
        </form>

      </main>
    </div>
  </div>
""")))}))
      }
    }
  }

  def render(form:Form[Screen]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Screen]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:07:00 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/screenViews/adminScreenEdit.scala.html
                  HASH: cc6323ac75a2d5279b7de8b359eadf389bfac4ee
                  MATRIX: 663->23|691->46|1036->73|1151->93|1181->98|1222->131|1261->133|1291->137|3935->2754|3970->2780|4022->2794|4070->2814|4218->2934|4236->2942|4264->2948|4335->2988|4383->3008|4670->3268|4707->3296|4759->3310|4807->3330|4953->3448|4971->3456|4999->3462|5085->3517|5133->3537|5420->3797|5457->3825|5509->3839|5557->3859|5701->3975|5719->3983|5747->3989|5833->4044|5881->4064
                  LINES: 24->2|25->3|30->4|35->4|37->6|37->6|37->6|38->7|100->69|100->69|100->69|101->70|101->70|101->70|101->70|102->71|103->72|108->77|108->77|108->77|109->78|109->78|109->78|109->78|110->79|111->80|116->85|116->85|116->85|117->86|117->86|117->86|117->86|118->87|119->88
                  -- GENERATED --
              */
          