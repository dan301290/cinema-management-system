
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.Movie
/*3.2*/import java.util

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[util.List[Movie],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(movies: util.List[Movie]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.28*/("""
"""),_display_(/*5.2*/main("Cinema-Frontend")/*5.25*/ {_display_(Seq[Any](format.raw/*5.27*/("""


  """),format.raw/*8.19*/("""

  """),format.raw/*10.3*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Cinema-System</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>

  """),format.raw/*34.17*/("""

  """),format.raw/*36.3*/("""<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        """),_display_(/*43.10*/for((movie, index) <- movies.zipWithIndex) yield /*43.52*/{_display_(Seq[Any](format.raw/*43.53*/("""
            """),_display_(/*44.14*/if(index == 0)/*44.28*/ {_display_(Seq[Any](format.raw/*44.30*/("""
                """),format.raw/*45.17*/("""<div class="carousel-item active">
                    <img class="first-slide" src=""""),_display_(/*46.52*/movie/*46.57*/.getImg_URL),format.raw/*46.68*/("""" alt="First slide">
                    <div class="container">
                        <div class="carousel-caption d-none d-md-block text-left">
                            <h1>"""),_display_(/*49.34*/movie/*49.39*/.getMovieName),format.raw/*49.52*/("""</h1>
                            <p>"""),_display_(/*50.33*/movie/*50.38*/.getShortDesc),format.raw/*50.51*/("""</p>
                            <p><a class="btn btn-lg btn-primary" href="#" role="button">Book Now</a></p>
                        </div>
                    </div>
                </div>
            """)))}),format.raw/*55.14*/("""
            """),_display_(/*56.14*/if((index != 0) && ( index <= 2))/*56.47*/ {_display_(Seq[Any](format.raw/*56.49*/("""
                """),format.raw/*57.17*/("""<div class="carousel-item">
                    <img class="first-slide" src=""""),_display_(/*58.52*/movie/*58.57*/.getImg_URL),format.raw/*58.68*/("""" alt="First slide">
                    <div class="container">
                        <div class="carousel-caption d-none d-md-block text-left">
                            <h1>"""),_display_(/*61.34*/movie/*61.39*/.getMovieName),format.raw/*61.52*/("""</h1>
                            <p>"""),_display_(/*62.33*/movie/*62.38*/.getShortDesc),format.raw/*62.51*/("""</p>
                            <p><a class="btn btn-lg btn-primary" href="#" role="button">Book Now</a></p>
                        </div>
                    </div>
                </div>
            """)))}),format.raw/*67.14*/("""
        """)))}),format.raw/*68.10*/("""

    """),format.raw/*70.5*/("""</div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  """),format.raw/*82.18*/("""

  """),format.raw/*84.3*/("""<div class="container marketing">

      <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-4">
        <img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
        <h2>Heading</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
        <img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
        <h2>Heading</h2>
        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
        <img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
        <h2>Heading</h2>
        <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->

    """),format.raw/*108.22*/("""

    """),format.raw/*110.5*/("""<hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
      </div>
      <div class="col-md-5">
        <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
      </div>
      <div class="col-md-5 order-md-1">
        <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
      </div>
      <div class="col-md-5">
        <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
      </div>
    </div>

    <hr class="featurette-divider">


      <!-- FOOTER -->
    <footer>
      <p class="float-right"><a href="#">Back to top</a></p>
      <p>&copy; 2017 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
    </footer>

  </div><!-- /.container -->


""")))}),format.raw/*158.2*/("""
"""))
      }
    }
  }

  def render(movies:util.List[Movie]): play.twirl.api.HtmlFormat.Appendable = apply(movies)

  def f:((util.List[Movie]) => play.twirl.api.HtmlFormat.Appendable) = (movies) => apply(movies)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:06:59 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/index.scala.html
                  HASH: 0180c1f8fbfd75d6a099845a224e9a0bf7003a98
                  MATRIX: 651->23|678->45|1009->64|1130->90|1158->93|1189->116|1228->118|1263->142|1296->148|2440->1278|2473->1284|2857->1641|2915->1683|2954->1684|2996->1699|3019->1713|3059->1715|3105->1733|3219->1820|3233->1825|3265->1836|3476->2020|3490->2025|3524->2038|3590->2077|3604->2082|3638->2095|3878->2304|3920->2319|3962->2352|4002->2354|4048->2372|4155->2452|4169->2457|4201->2468|4412->2652|4426->2657|4460->2670|4526->2709|4540->2714|4574->2727|4814->2936|4856->2947|4891->2955|5379->3430|5412->3436|7416->5428|7452->5436|9757->7710
                  LINES: 24->2|25->3|30->4|35->4|36->5|36->5|36->5|39->8|41->10|65->34|67->36|74->43|74->43|74->43|75->44|75->44|75->44|76->45|77->46|77->46|77->46|80->49|80->49|80->49|81->50|81->50|81->50|86->55|87->56|87->56|87->56|88->57|89->58|89->58|89->58|92->61|92->61|92->61|93->62|93->62|93->62|98->67|99->68|101->70|113->82|115->84|139->108|141->110|189->158
                  -- GENERATED --
              */
          