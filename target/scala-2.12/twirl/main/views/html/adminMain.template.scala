
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object adminMain extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /*
* This template is called from the `index` template. This template
* handles the rendering of the page header and body tags. It takes
* two arguments, a `String` for the title of the page and an `Html`
* object to insert into the body of the page.
*/
  def apply/*7.2*/(title: String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*7.32*/("""

"""),format.raw/*9.1*/("""<!DOCTYPE html>
<html lang="en">
  <head>

    """),format.raw/*13.58*/("""
    """),format.raw/*14.5*/("""<title>"""),_display_(/*14.13*/title),format.raw/*14.18*/("""</title>
    <link rel="stylesheet" media="screen" href=""""),_display_(/*15.50*/routes/*15.56*/.Assets.versioned("stylesheets/main.css")),format.raw/*15.97*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(/*16.50*/routes/*16.56*/.Assets.versioned("stylesheets/bootstrap.min.css")),format.raw/*16.106*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(/*17.50*/routes/*17.56*/.Assets.versioned("stylesheets/dashboard.css")),format.raw/*17.102*/("""">
    <link rel="shortcut icon" type="image/png" href=""""),_display_(/*18.55*/routes/*18.61*/.Assets.versioned("images/favicon.png")),format.raw/*18.100*/("""">

  </head>
  <body>
    """),format.raw/*23.27*/("""
    """),_display_(/*24.6*/content),format.raw/*24.13*/("""

    """),format.raw/*26.5*/("""<script src=""""),_display_(/*26.19*/routes/*26.25*/.Assets.versioned("javascripts/main.js")),format.raw/*26.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(/*27.19*/routes/*27.25*/.Assets.versioned("javascripts/jquery-3.2.1.slim.min.js")),format.raw/*27.82*/("""" type="text/javascript"></script>
    <script src=""""),_display_(/*28.19*/routes/*28.25*/.Assets.versioned("javascripts/popper.min.js")),format.raw/*28.71*/("""" type="text/javascript"></script>
    <script src=""""),_display_(/*29.19*/routes/*29.25*/.Assets.versioned("javascripts/bootstrap.min.js")),format.raw/*29.74*/("""" type="text/javascript"></script>
    <script src=""""),_display_(/*30.19*/routes/*30.25*/.Assets.versioned("javascripts/holder.min.js")),format.raw/*30.71*/("""" type="text/javascript"></script>


  <script type="text/javascript">
                    $(function() """),format.raw/*34.34*/("""{"""),format.raw/*34.35*/("""
                      """),format.raw/*35.23*/("""$("#img_URL").on("change", function()
                      """),format.raw/*36.23*/("""{"""),format.raw/*36.24*/("""
                        """),format.raw/*37.25*/("""var imageURL = $("#img_URL").val();

                        $("#image_preview").attr("src",imageURL);


                      """),format.raw/*42.23*/("""}"""),format.raw/*42.24*/(""");
                    """),format.raw/*43.21*/("""}"""),format.raw/*43.22*/(""");
            </script>

  </body>
</html>
"""))
      }
    }
  }

  def render(title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:06:59 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/adminMain.scala.html
                  HASH: becc7b0c8b997799de78acedbfd2c02bdf72552c
                  MATRIX: 1211->261|1336->291|1366->295|1445->399|1478->405|1513->413|1539->418|1625->477|1640->483|1702->524|1782->577|1797->583|1869->633|1949->686|1964->692|2032->738|2117->796|2132->802|2193->841|2252->958|2285->965|2313->972|2348->980|2389->994|2404->1000|2465->1040|2546->1094|2561->1100|2639->1157|2720->1211|2735->1217|2802->1263|2883->1317|2898->1323|2968->1372|3049->1426|3064->1432|3131->1478|3267->1586|3296->1587|3348->1611|3437->1672|3466->1673|3520->1699|3680->1831|3709->1832|3761->1856|3790->1857
                  LINES: 33->7|38->7|40->9|44->13|45->14|45->14|45->14|46->15|46->15|46->15|47->16|47->16|47->16|48->17|48->17|48->17|49->18|49->18|49->18|53->23|54->24|54->24|56->26|56->26|56->26|56->26|57->27|57->27|57->27|58->28|58->28|58->28|59->29|59->29|59->29|60->30|60->30|60->30|64->34|64->34|65->35|66->36|66->36|67->37|72->42|72->42|73->43|73->43
                  -- GENERATED --
              */
          