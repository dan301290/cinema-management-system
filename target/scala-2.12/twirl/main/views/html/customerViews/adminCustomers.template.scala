
package views.html.customerViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*1.2*/import models.Customer
/*2.2*/import java.util

object adminCustomers extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[util.List[Customer],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(customers: util.List[Customer]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.34*/("""

"""),_display_(/*5.2*/adminMain("Cinema-Admin-Customers")/*5.37*/ {_display_(Seq[Any](format.raw/*5.39*/("""
  """),format.raw/*6.3*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Dashboard - Cinema Manager</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Help</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link" href="/admin">Overview</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/movies">Movies</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/showings">Showings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/bookings">Bookings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="/admin/customers">Customers <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/screens">Screens</a>
          </li>
        </ul>
      </nav>

      <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
        <h1>Dashboard - Customers</h1>

        <section class="row text-center placeholders">
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <div class="text-muted">current showing capaicty</div>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">current seats filled</span>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">Something else</span>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">Something else</span>
          </div>
        </section>

        <h2>Customers</h2>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>customerId</th>
                <th>firstName</th>
                <th>lastName</th>
                <th>email</th>
                <th>phone</th>
                <th>address</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
              <tbody>
              """),_display_(/*101.16*/for(customer <- customers) yield /*101.42*/{_display_(Seq[Any](format.raw/*101.43*/("""
                  """),format.raw/*102.19*/("""<tr>
                      <td>"""),_display_(/*103.28*/customer/*103.36*/.getCustomerId),format.raw/*103.50*/("""</td>
                      <td>"""),_display_(/*104.28*/customer/*104.36*/.getFirstName),format.raw/*104.49*/("""</td>
                      <td>"""),_display_(/*105.28*/customer/*105.36*/.getLastName),format.raw/*105.48*/("""</td>
                      <td>"""),_display_(/*106.28*/customer/*106.36*/.getEmail),format.raw/*106.45*/("""</td>
                      <td>"""),_display_(/*107.28*/customer/*107.36*/.getPhone),format.raw/*107.45*/("""</td>
                      <td>"""),_display_(/*108.28*/customer/*108.36*/.getAddress),format.raw/*108.47*/("""</td>
                      <th><a class="btn btn-primary" href="customers/edit/"""),_display_(/*109.76*/customer/*109.84*/.getCustomerId),format.raw/*109.98*/("""" role="button">Edit</a></th>
                      <th><a class="btn btn-primary" href="customers/destroy/"""),_display_(/*110.79*/customer/*110.87*/.getCustomerId),format.raw/*110.101*/("""" role="button">Delete</a></th>

                  </tr>
              """)))}),format.raw/*113.16*/("""
              """),format.raw/*114.15*/("""</tbody>
          </table>
        </div>

          <a class="btn btn-primary" href="customers/create" role="button">Add New Customer</a>

      </main>
    </div>
  </div>
""")))}))
      }
    }
  }

  def render(customers:util.List[Customer]): play.twirl.api.HtmlFormat.Appendable = apply(customers)

  def f:((util.List[Customer]) => play.twirl.api.HtmlFormat.Appendable) = (customers) => apply(customers)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:07:00 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/customerViews/adminCustomers.scala.html
                  HASH: 5e5b19a9308c944c9990373b8926a9651f303c75
                  MATRIX: 665->1|695->26|1038->45|1165->77|1195->82|1238->117|1277->119|1307->123|5642->4430|5685->4456|5725->4457|5774->4477|5835->4510|5853->4518|5889->4532|5951->4566|5969->4574|6004->4587|6066->4621|6084->4629|6118->4641|6180->4675|6198->4683|6229->4692|6291->4726|6309->4734|6340->4743|6402->4777|6420->4785|6453->4796|6563->4878|6581->4886|6617->4900|6754->5009|6772->5017|6809->5031|6916->5106|6961->5122
                  LINES: 24->1|25->2|30->3|35->3|37->5|37->5|37->5|38->6|133->101|133->101|133->101|134->102|135->103|135->103|135->103|136->104|136->104|136->104|137->105|137->105|137->105|138->106|138->106|138->106|139->107|139->107|139->107|140->108|140->108|140->108|141->109|141->109|141->109|142->110|142->110|142->110|145->113|146->114
                  -- GENERATED --
              */
          