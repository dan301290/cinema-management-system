
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.User
/*3.2*/import views.html.helper

object login extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[User],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(form: Form[User]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.20*/("""


"""),format.raw/*7.1*/("""<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Signin Template for Bootstrap</title>
      <!-- Bootstrap core CSS -->
    <link rel="stylesheet" media="screen" href=""""),_display_(/*12.50*/routes/*12.56*/.Assets.versioned("stylesheets/bootstrap.min.css")),format.raw/*12.106*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(/*13.50*/routes/*13.56*/.Assets.versioned("stylesheets/signin.css")),format.raw/*13.99*/("""">
  </head>

  <body>

    <div class="container">


        <form class="form-signin" action="/authenticate" method="GET">
            <h2 class="form-signin-heading">Please sign in</h2>
            <label for="userName" class="sr-only">User Name</label>
            <input type="text" id="userName" name="userName" class="form-control" placeholder="userName" required autofocus>
            <label for="password" class="sr-only">Password</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>

        """),_display_(/*35.10*/for(key <- Array("danger", "info", "success", "warning")) yield /*35.67*/{_display_(Seq[Any](format.raw/*35.68*/("""
            """),_display_(/*36.14*/if(flash().containsKey(key))/*36.42*/{_display_(Seq[Any](format.raw/*36.43*/("""

                """),format.raw/*38.17*/("""<div class="alert alert-"""),_display_(/*38.42*/key),format.raw/*38.45*/(""" """),format.raw/*38.46*/("""alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    """),_display_(/*42.22*/flash/*42.27*/.get(key)),format.raw/*42.36*/("""
                """),format.raw/*43.17*/("""</div>
            """)))}),format.raw/*44.14*/("""
        """)))}),format.raw/*45.10*/("""


    """),format.raw/*48.5*/("""</div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src=""""),_display_(/*53.19*/routes/*53.25*/.Assets.versioned("javascripts/main.js")),format.raw/*53.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(/*54.19*/routes/*54.25*/.Assets.versioned("javascripts/jquery-3.2.1.slim.min.js")),format.raw/*54.82*/("""" type="text/javascript"></script>
    <script src=""""),_display_(/*55.19*/routes/*55.25*/.Assets.versioned("javascripts/popper.min.js")),format.raw/*55.71*/("""" type="text/javascript"></script>
    <script src=""""),_display_(/*56.19*/routes/*56.25*/.Assets.versioned("javascripts/bootstrap.min.js")),format.raw/*56.74*/("""" type="text/javascript"></script>
  </body>
</html>"""))
      }
    }
  }

  def render(form:Form[User]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[User]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:06:59 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/login.scala.html
                  HASH: 97e683f5fd2ef6bdae96220b7a7489b5d933a131
                  MATRIX: 651->23|677->44|1010->71|1123->89|1155->95|1361->274|1376->280|1448->330|1528->383|1543->389|1607->432|2521->1319|2594->1376|2633->1377|2675->1392|2712->1420|2751->1421|2799->1441|2851->1466|2875->1469|2904->1470|3190->1729|3204->1734|3234->1743|3280->1761|3332->1782|3374->1793|3411->1803|3584->1949|3599->1955|3660->1995|3741->2049|3756->2055|3834->2112|3915->2166|3930->2172|3997->2218|4078->2272|4093->2278|4163->2327
                  LINES: 24->2|25->3|30->4|35->4|38->7|43->12|43->12|43->12|44->13|44->13|44->13|66->35|66->35|66->35|67->36|67->36|67->36|69->38|69->38|69->38|69->38|73->42|73->42|73->42|74->43|75->44|76->45|79->48|84->53|84->53|84->53|85->54|85->54|85->54|86->55|86->55|86->55|87->56|87->56|87->56
                  -- GENERATED --
              */
          