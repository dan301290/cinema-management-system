
package views.html.movieViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.Movie
/*3.2*/import views.html.helper

object adminMovieAdd extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Movie],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(form: Form[Movie]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.21*/("""

"""),_display_(/*6.2*/adminMain("Cinema-Admin-Movies")/*6.34*/ {_display_(Seq[Any](format.raw/*6.36*/("""
  """),format.raw/*7.3*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Dashboard - Cinema Manager</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Help</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link" href="/admin">Overview</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="/admin/movies">Movies <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/showings">Showings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/bookings">Bookings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/customers">Customers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/screens">Screens</a>
          </li>
        </ul>
      </nav>

      <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
        <h1>Dashboard - Add Movie</h1>

          <form class="form-AddMovie" action="/admin/movies/save" method="GET">
              <section class="row placeholders">
                  <div class="col-6 col-sm-6 placeholder">

                      <div class="form-group">
                          <label for="movieName">Movie Name</label>
                          <input type="text" class="form-control" id="movieName" name="movieName" aria-describedby="movieNameHelp" placeholder="movieName" required autofocus>
                              <!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                      </div>

                      <div class="form-group">
                          <label for="ageRating">Age Rating</label>
                          <select class="form-control" id="ageRating" name="ageRating" aria-describedby="ageRatingHelp" required>
                              <option>U - Suitable for all</option>
                              <option>PG - Parental guidance</option>
                              <option>12A - Cinema release suitable for 12 years and over</option>
                              <option>15 - Suitable only for 15 years and over</option>
                              <option>18 - Suitable only for adults</option>
                          </select>
                      </div>

                      <div class="form-group">
                          <label for="shortDesc">Short Description</label>
                          <textarea class="form-control" id="shortDesc" name="shortDesc" aria-describedby="shortDescHelp" placeholder="shortDesc" required rows="3"></textarea>
                      </div>

                      <div class="form-group">
                          <label for="plot">Plot</label>
                          <textarea class="form-control" id="plot" name="plot" aria-describedby="plotHelp" placeholder="plot" required rows="3"></textarea>
                      </div>

                      <div class="col-xs-10 col-sm-4 col-md-4 col-lg-4">
                          <button class="btn btn-lg btn-primary btn-block" type="submit">Add Movie</button>
                      </div>

                  </div>
                  <div class="col-6 col-sm-6 placeholder">

                      <div class="form-group">
                          <label for="year">Year</label>
                          <input type="number" class="form-control" id="year" name="year" aria-describedby="yearHelp" placeholder="year" required>
                              <!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                      </div>

                      <div class="form-group">
                          <label for="genre">Genre</label>
                          <input type="text" class="form-control" id="genre" name="genre" aria-describedby="genreHelp" placeholder="genre" required>
                              <!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                      </div>

                      <div class="form-group">
                          <label for="released">Released</label>
                          <input type="date" class="form-control" id="released" name="released" aria-describedby="releasedHelp" placeholder="released" required>
                              <!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                      </div>

                      <div class="form-group">
                          <label for="director">Director</label>
                          <input type="text" class="form-control" id="director" name="director" aria-describedby="directorHelp" placeholder="director" required>
                              <!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                      </div>

                      <div class="form-group">
                          <label for="runtime">Runtime</label>
                          <input type="text" class="form-control" id="runtime" name="runtime" aria-describedby="runtimeHelp" placeholder="runtime" required>
                              <!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                      </div>


                      <div class="form-group">
                          <label for="img_URL">Image URL</label>
                          <input type="url" class="form-control" id="img_URL" name="img_URL" aria-describedby="img_URLHelp" placeholder="img_URL" required>
                          <img src="" class="img-fluid" alt="Image" id="image_preview">
                      </div>

                  </div>
              </section>

              """),_display_(/*141.16*/for(key <- Array("danger", "info", "success", "warning")) yield /*141.73*/{_display_(Seq[Any](format.raw/*141.74*/("""
                  """),_display_(/*142.20*/if(flash().containsKey(key))/*142.48*/{_display_(Seq[Any](format.raw/*142.49*/("""

                      """),format.raw/*144.23*/("""<div class="alert alert-"""),_display_(/*144.48*/key),format.raw/*144.51*/(""" """),format.raw/*144.52*/("""alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                          """),_display_(/*148.28*/flash/*148.33*/.get(key)),format.raw/*148.42*/("""
                      """),format.raw/*149.23*/("""</div>
                  """)))}),format.raw/*150.20*/("""
              """)))}),format.raw/*151.16*/("""

          """),format.raw/*153.11*/("""</form>

      </main>
    </div>
  </div>
""")))}))
      }
    }
  }

  def render(form:Form[Movie]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Movie]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 15:14:05 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/movieViews/adminMovieAdd.scala.html
                  HASH: a3f35cd4d4ee0e42a924c5cafc8c998f2261e12f
                  MATRIX: 662->23|689->45|1031->72|1145->91|1175->96|1215->128|1254->130|1284->134|8652->7474|8726->7531|8766->7532|8815->7553|8853->7581|8893->7582|8948->7608|9001->7633|9026->7636|9056->7637|9367->7920|9382->7925|9413->7934|9466->7958|9525->7985|9574->8002|9617->8016
                  LINES: 24->2|25->3|30->4|35->4|37->6|37->6|37->6|38->7|172->141|172->141|172->141|173->142|173->142|173->142|175->144|175->144|175->144|175->144|179->148|179->148|179->148|180->149|181->150|182->151|184->153
                  -- GENERATED --
              */
          