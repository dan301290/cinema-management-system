
package views.html.movieViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.Movie
/*3.2*/import java.util

object adminMovies extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[util.List[Movie],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(movies: util.List[Movie]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.28*/("""

"""),_display_(/*6.2*/adminMain("Cinema-Admin-Movies")/*6.34*/ {_display_(Seq[Any](format.raw/*6.36*/("""
  """),format.raw/*7.3*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Dashboard - Cinema Manager</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Help</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link" href="/admin">Overview</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="/admin/movies">Movies <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/showings">Showings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/bookings">Bookings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/customers">Customers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/screens">Screens</a>
          </li>
        </ul>
      </nav>

      <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
        <h1>Dashboard - Movies</h1>

        """),_display_(/*63.10*/for(key <- Array("danger", "info", "success", "warning")) yield /*63.67*/{_display_(Seq[Any](format.raw/*63.68*/("""
          """),_display_(/*64.12*/if(flash().containsKey(key))/*64.40*/{_display_(Seq[Any](format.raw/*64.41*/("""

            """),format.raw/*66.13*/("""<div class="alert alert-"""),_display_(/*66.38*/key),format.raw/*66.41*/(""" """),format.raw/*66.42*/("""alert-dismissible fade show" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              """),_display_(/*70.16*/flash/*70.21*/.get(key)),format.raw/*70.30*/("""
            """),format.raw/*71.13*/("""</div>
          """)))}),format.raw/*72.12*/("""
        """)))}),format.raw/*73.10*/("""

        """),format.raw/*75.9*/("""<section class="row text-center placeholders">
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <div class="text-muted">current showing capaicty</div>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">current seats filled</span>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">Something else</span>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">Something else</span>
          </div>
        </section>

        <h2>Movies</h2>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Movie Id</th>
                <th>Title </th>
                <th>Age Rating</th>
                <th>Short Desc</th>
                <th>Image</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
               """),_display_(/*113.17*/for(movie <- movies) yield /*113.37*/{_display_(Seq[Any](format.raw/*113.38*/("""
                    """),format.raw/*114.21*/("""<tr>
                        <td>"""),_display_(/*115.30*/movie/*115.35*/.getMovieId),format.raw/*115.46*/("""</td>
                        <td>"""),_display_(/*116.30*/movie/*116.35*/.getMovieName),format.raw/*116.48*/("""</td>
                        <td>"""),_display_(/*117.30*/movie/*117.35*/.getAgeRating),format.raw/*117.48*/("""</td>
                        <td>"""),_display_(/*118.30*/movie/*118.35*/.getShortDesc),format.raw/*118.48*/("""</td>
                        <td><img src=""""),_display_(/*119.40*/movie/*119.45*/.getImg_URL),format.raw/*119.56*/("""" class="img-fluid" alt="Image"></td>
                        <th><a class="btn btn-primary" href="movies/edit/"""),_display_(/*120.75*/movie/*120.80*/.getMovieId),format.raw/*120.91*/("""" role="button">Edit</a></th>
                        <th><a class="btn btn-primary" href="movies/destroy/"""),_display_(/*121.78*/movie/*121.83*/.getMovieId),format.raw/*121.94*/("""" role="button">Delete</a></th>

                    </tr>
                """)))}),format.raw/*124.18*/("""
            """),format.raw/*125.13*/("""</tbody>
          </table>
        </div>

          <a class="btn btn-primary" href="movies/create" role="button">Add New Movie</a>


      </main>
    </div>
  </div>
""")))}))
      }
    }
  }

  def render(movies:util.List[Movie]): play.twirl.api.HtmlFormat.Appendable = apply(movies)

  def f:((util.List[Movie]) => play.twirl.api.HtmlFormat.Appendable) = (movies) => apply(movies)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:07:00 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/movieViews/adminMovies.scala.html
                  HASH: dd51c4dde102df469e028584d00a066bfd587d43
                  MATRIX: 662->23|689->45|1026->64|1147->90|1177->95|1217->127|1256->129|1286->133|3635->2455|3708->2512|3747->2513|3787->2526|3824->2554|3863->2555|3907->2571|3959->2596|3983->2599|4012->2600|4272->2833|4286->2838|4316->2847|4358->2861|4408->2880|4450->2891|4489->2903|6464->4850|6501->4870|6541->4871|6592->4893|6655->4928|6670->4933|6703->4944|6767->4980|6782->4985|6817->4998|6881->5034|6896->5039|6931->5052|6995->5088|7010->5093|7045->5106|7119->5152|7134->5157|7167->5168|7308->5281|7323->5286|7356->5297|7492->5405|7507->5410|7540->5421|7651->5500|7694->5514
                  LINES: 24->2|25->3|30->4|35->4|37->6|37->6|37->6|38->7|94->63|94->63|94->63|95->64|95->64|95->64|97->66|97->66|97->66|97->66|101->70|101->70|101->70|102->71|103->72|104->73|106->75|144->113|144->113|144->113|145->114|146->115|146->115|146->115|147->116|147->116|147->116|148->117|148->117|148->117|149->118|149->118|149->118|150->119|150->119|150->119|151->120|151->120|151->120|152->121|152->121|152->121|155->124|156->125
                  -- GENERATED --
              */
          