
package views.html.movieViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.Movie
/*3.2*/import views.html.helper

object adminMovieEdit extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[Form[Movie],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(form: Form[Movie]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.21*/("""

"""),_display_(/*6.2*/adminMain("Cinema-Admin-Movies")/*6.34*/ {_display_(Seq[Any](format.raw/*6.36*/("""
    """),format.raw/*7.5*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Dashboard - Cinema Manager</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Settings</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Help</a>
                </li>
            </ul>
            <form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin">Overview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/admin/movies">Movies <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/showings">Showings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/bookings">Bookings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/customers">Customers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/screens">Screens</a>
                    </li>
                </ul>
            </nav>

            <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
                <h1>Dashboard - Edit Movie</h1>


                <form class="form-AddMovie" action="/admin/movies/update" method="GET">
                    <section class="row placeholders">
                        <div class="col-6 col-sm-6 placeholder">

                            <div class="form-group">
                                <label for="movieId">Movie Id</label>
                                """),_display_(/*70.34*/defining(form("movieId"))/*70.59*/ { uidField =>_display_(Seq[Any](format.raw/*70.73*/("""
                                    """),format.raw/*71.37*/("""<input readonly  type="number" class="form-control" id="movieId" name="movieId" aria-describedby="movieIdHelp" value=""""),_display_(/*71.156*/uidField/*71.164*/.value),format.raw/*71.170*/("""" required autofocus>
                                """)))}),format.raw/*72.34*/("""
                                    """),format.raw/*73.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="movieName">Movie Name</label>
                                """),_display_(/*78.34*/defining(form("movieName"))/*78.61*/ { uidField =>_display_(Seq[Any](format.raw/*78.75*/("""
                                    """),format.raw/*79.37*/("""<input type="text" class="form-control" id="movieName" name="movieName" aria-describedby="movieNameHelp" value=""""),_display_(/*79.150*/uidField/*79.158*/.value),format.raw/*79.164*/("""" placeholder="movieName" required>
                                """)))}),format.raw/*80.34*/("""
                                    """),format.raw/*81.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="ageRating">Age Rating</label>
                                """),_display_(/*86.34*/defining(form("ageRating"))/*86.61*/ { uidField =>_display_(Seq[Any](format.raw/*86.75*/("""
                                    """),format.raw/*87.37*/("""<select class="form-control" id="ageRating" name="ageRating" aria-describedby="ageRatingHelp" required>
                                        <option>"""),_display_(/*88.50*/uidField/*88.58*/.value),format.raw/*88.64*/("""</option>
                                        <option>U - Suitable for all</option>
                                        <option>PG - Parental guidance</option>
                                        <option>12A - Cinema release suitable for 12 years and over</option>
                                        <option>15 - Suitable only for 15 years and over</option>
                                        <option>18 - Suitable only for adults</option>
                                    </select>
                                """)))}),format.raw/*95.34*/("""
                            """),format.raw/*96.29*/("""</div>

                            <div class="form-group">
                                <label for="shortDesc">Short Description</label>
                                """),_display_(/*100.34*/defining(form("shortDesc"))/*100.61*/ { uidField =>_display_(Seq[Any](format.raw/*100.75*/("""
                                    """),format.raw/*101.37*/("""<textarea class="form-control" id="shortDesc" name="shortDesc" aria-describedby="shortDescHelp" placeholder="shortDesc" required rows="3">"""),_display_(/*101.176*/uidField/*101.184*/.value),format.raw/*101.190*/("""</textarea>
                                """)))}),format.raw/*102.34*/("""
                            """),format.raw/*103.29*/("""</div>

                            <div class="form-group">
                                <label for="plot">Plot</label>
                                """),_display_(/*107.34*/defining(form("plot"))/*107.56*/ { uidField =>_display_(Seq[Any](format.raw/*107.70*/("""
                                    """),format.raw/*108.37*/("""<textarea class="form-control" id="plot" name="plot" aria-describedby="longDescHelp" placeholder="plot" required rows="3">"""),_display_(/*108.160*/uidField/*108.168*/.value),format.raw/*108.174*/("""</textarea>
                                """)))}),format.raw/*109.34*/("""
                            """),format.raw/*110.29*/("""</div>

                            <div class="col-xs-10 col-sm-4 col-md-4 col-lg-4">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Update Movie</button>
                            </div>

                        </div>
                        <div class="col-6 col-sm-6 placeholder">

                            <div class="form-group">
                                <label for="year">Year</label>
                                """),_display_(/*121.34*/defining(form("year"))/*121.56*/ { uidField =>_display_(Seq[Any](format.raw/*121.70*/("""
                                    """),format.raw/*122.37*/("""<input type="number" class="form-control" id="year" name="year" aria-describedby="yearHelp" value=""""),_display_(/*122.137*/uidField/*122.145*/.value),format.raw/*122.151*/("""" placeholder="year" required>
                                """)))}),format.raw/*123.34*/("""
                                    """),format.raw/*124.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="genre">Genre</label>
                                """),_display_(/*129.34*/defining(form("genre"))/*129.57*/ { uidField =>_display_(Seq[Any](format.raw/*129.71*/("""
                                    """),format.raw/*130.37*/("""<input type="text" class="form-control" id="genre" name="genre" aria-describedby="genreHelp" value=""""),_display_(/*130.138*/uidField/*130.146*/.value),format.raw/*130.152*/("""" placeholder="genre" required>
                                """)))}),format.raw/*131.34*/("""
                                    """),format.raw/*132.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="released">Released</label>
                                """),_display_(/*137.34*/defining(form("released"))/*137.60*/ { uidField =>_display_(Seq[Any](format.raw/*137.74*/("""
                                    """),format.raw/*138.37*/("""<input type="date" class="form-control" id="released" name="released" aria-describedby="releasedHelp" value=""""),_display_(/*138.147*/uidField/*138.155*/.value),format.raw/*138.161*/("""" placeholder="released" required>
                                """)))}),format.raw/*139.34*/("""
                                    """),format.raw/*140.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="director">Director</label>
                                """),_display_(/*145.34*/defining(form("director"))/*145.60*/ { uidField =>_display_(Seq[Any](format.raw/*145.74*/("""
                                    """),format.raw/*146.37*/("""<input type="text" class="form-control" id="director" name="director" aria-describedby="directorHelp" value=""""),_display_(/*146.147*/uidField/*146.155*/.value),format.raw/*146.161*/("""" placeholder="director" required>
                                """)))}),format.raw/*147.34*/("""
                                    """),format.raw/*148.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>

                            <div class="form-group">
                                <label for="runtime">Runtime</label>
                                """),_display_(/*153.34*/defining(form("runtime"))/*153.59*/ { uidField =>_display_(Seq[Any](format.raw/*153.73*/("""
                                    """),format.raw/*154.37*/("""<input type="text" class="form-control" id="runtime" name="runtime" aria-describedby="runtimeHelp" value=""""),_display_(/*154.144*/uidField/*154.152*/.value),format.raw/*154.158*/("""" placeholder="runtime" required>
                                """)))}),format.raw/*155.34*/("""
                                    """),format.raw/*156.37*/("""<!-- <small id="movieNameHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                            </div>


                            <div class="form-group">
                                <label for="img_URL">Image URL</label>
                                """),_display_(/*162.34*/defining(form("img_URL"))/*162.59*/ { uidField =>_display_(Seq[Any](format.raw/*162.73*/("""
                                    """),format.raw/*163.37*/("""<input type="url" class="form-control" id="img_URL" name="img_URL" aria-describedby="img_URLHelp" placeholder="img_URL" value=""""),_display_(/*163.165*/uidField/*163.173*/.value),format.raw/*163.179*/("""" required>
                                    <img src=""""),_display_(/*164.48*/uidField/*164.56*/.value),format.raw/*164.62*/("""" class="img-fluid" alt="Image" id="image_preview">
                                """)))}),format.raw/*165.34*/("""
                            """),format.raw/*166.29*/("""</div>

                        </div>
                    </section>

                    """),_display_(/*171.22*/for(key <- Array("danger", "info", "success", "warning")) yield /*171.79*/{_display_(Seq[Any](format.raw/*171.80*/("""
                        """),_display_(/*172.26*/if(flash().containsKey(key))/*172.54*/{_display_(Seq[Any](format.raw/*172.55*/("""

                            """),format.raw/*174.29*/("""<div class="alert alert-"""),_display_(/*174.54*/key),format.raw/*174.57*/(""" """),format.raw/*174.58*/("""alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                """),_display_(/*178.34*/flash/*178.39*/.get(key)),format.raw/*178.48*/("""
                            """),format.raw/*179.29*/("""</div>
                        """)))}),format.raw/*180.26*/("""
                    """)))}),format.raw/*181.22*/("""

                """),format.raw/*183.17*/("""</form>

            </main>
        </div>
    </div>
""")))}))
      }
    }
  }

  def render(form:Form[Movie]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Movie]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:07:00 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/movieViews/adminMovieEdit.scala.html
                  HASH: e8022f93d18ce628c493beca1cf4f24461e925b0
                  MATRIX: 662->23|689->45|1032->72|1146->91|1176->96|1216->128|1255->130|1287->136|4416->3238|4450->3263|4502->3277|4568->3315|4715->3434|4733->3442|4761->3448|4848->3504|4914->3542|5260->3861|5296->3888|5348->3902|5414->3940|5555->4053|5573->4061|5601->4067|5702->4137|5768->4175|6114->4494|6150->4521|6202->4535|6268->4573|6449->4727|6466->4735|6493->4741|7072->5289|7130->5319|7337->5498|7374->5525|7427->5539|7494->5577|7662->5716|7681->5724|7710->5730|7788->5776|7847->5806|8036->5967|8068->5989|8121->6003|8188->6041|8340->6164|8359->6172|8388->6178|8466->6224|8525->6254|9050->6751|9082->6773|9135->6787|9202->6825|9331->6925|9350->6933|9379->6939|9476->7004|9543->7042|9881->7352|9914->7375|9967->7389|10034->7427|10164->7528|10183->7536|10212->7542|10310->7608|10377->7646|10721->7962|10757->7988|10810->8002|10877->8040|11016->8150|11035->8158|11064->8164|11165->8233|11232->8271|11576->8587|11612->8613|11665->8627|11732->8665|11871->8775|11890->8783|11919->8789|12020->8858|12087->8896|12429->9210|12464->9235|12517->9249|12584->9287|12720->9394|12739->9402|12768->9408|12868->9476|12935->9514|13281->9832|13316->9857|13369->9871|13436->9909|13593->10037|13612->10045|13641->10051|13729->10111|13747->10119|13775->10125|13893->10211|13952->10241|14077->10338|14151->10395|14191->10396|14246->10423|14284->10451|14324->10452|14385->10484|14438->10509|14463->10512|14493->10513|14828->10820|14843->10825|14874->10834|14933->10864|14998->10897|15053->10920|15102->10940
                  LINES: 24->2|25->3|30->4|35->4|37->6|37->6|37->6|38->7|101->70|101->70|101->70|102->71|102->71|102->71|102->71|103->72|104->73|109->78|109->78|109->78|110->79|110->79|110->79|110->79|111->80|112->81|117->86|117->86|117->86|118->87|119->88|119->88|119->88|126->95|127->96|131->100|131->100|131->100|132->101|132->101|132->101|132->101|133->102|134->103|138->107|138->107|138->107|139->108|139->108|139->108|139->108|140->109|141->110|152->121|152->121|152->121|153->122|153->122|153->122|153->122|154->123|155->124|160->129|160->129|160->129|161->130|161->130|161->130|161->130|162->131|163->132|168->137|168->137|168->137|169->138|169->138|169->138|169->138|170->139|171->140|176->145|176->145|176->145|177->146|177->146|177->146|177->146|178->147|179->148|184->153|184->153|184->153|185->154|185->154|185->154|185->154|186->155|187->156|193->162|193->162|193->162|194->163|194->163|194->163|194->163|195->164|195->164|195->164|196->165|197->166|202->171|202->171|202->171|203->172|203->172|203->172|205->174|205->174|205->174|205->174|209->178|209->178|209->178|210->179|211->180|212->181|214->183
                  -- GENERATED --
              */
          