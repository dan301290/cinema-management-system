
package views.html.bookingViews

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._
/*2.2*/import models.Booking
/*3.2*/import java.util

object adminBookings extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[util.List[Booking],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*4.2*/(bookings: util.List[Booking]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*4.32*/("""

"""),_display_(/*6.2*/adminMain("Cinema-Admin-Bookings")/*6.36*/ {_display_(Seq[Any](format.raw/*6.38*/("""
  """),format.raw/*7.3*/("""<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Dashboard - Cinema Manager</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Settings</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Help</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link" href="/admin">Overview</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/movies">Movies</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/showings">Showings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="/admin/bookings">Bookings <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/customers">Customers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/screens">Screens</a>
          </li>
        </ul>
      </nav>

      <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
        <h1>Dashboard - Bookings</h1>

        <section class="row text-center placeholders">
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <div class="text-muted">current showing capaicty</div>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">current seats filled</span>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAAJ12AAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">Something else</span>
          </div>
          <div class="col-6 col-sm-3 placeholder">
            <img src="data:image/gif;base64,R0lGODlhAQABAIABAADcgwAAACwAAAAAAQABAAACAkQBADs=" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
            <h4>Label</h4>
            <span class="text-muted">Something else</span>
          </div>
        </section>

        <h2>latest Bookings</h2>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>bookingId</th>
                <th>showingId</th>
                <th>seatNum</th>
                <th>customerId</th>
                <th>Date</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
            """),_display_(/*101.14*/for(booking <- bookings) yield /*101.38*/{_display_(Seq[Any](format.raw/*101.39*/("""
              """),format.raw/*102.15*/("""<tr>
                <td>"""),_display_(/*103.22*/booking/*103.29*/.getBookingId),format.raw/*103.42*/("""</td>
                <td>"""),_display_(/*104.22*/booking/*104.29*/.getShowingId),format.raw/*104.42*/("""</td>
                <td>"""),_display_(/*105.22*/booking/*105.29*/.getCustomerId),format.raw/*105.43*/("""</td>
                <td>"""),_display_(/*106.22*/booking/*106.29*/.getSeatNum),format.raw/*106.40*/("""</td>
                <td>"""),_display_(/*107.22*/booking/*107.29*/.getDate),format.raw/*107.37*/("""</td>
                <th><a class="btn btn-primary" href="bookings/edit/"""),_display_(/*108.69*/booking/*108.76*/.getBookingId),format.raw/*108.89*/("""" role="button">Edit</a></th>
                <th><a class="btn btn-primary" href="bookings/destroy/"""),_display_(/*109.72*/booking/*109.79*/.getBookingId),format.raw/*109.92*/("""" role="button">Delete</a></th>

              </tr>
            """)))}),format.raw/*112.14*/("""
            """),format.raw/*113.13*/("""</tbody>
          </table>
        </div>

        <a class="btn btn-primary" href="bookings/create" role="button">Add New Booking</a>

      </main>
    </div>
  </div>
""")))}))
      }
    }
  }

  def render(bookings:util.List[Booking]): play.twirl.api.HtmlFormat.Appendable = apply(bookings)

  def f:((util.List[Booking]) => play.twirl.api.HtmlFormat.Appendable) = (bookings) => apply(bookings)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Thu Oct 12 12:07:00 BST 2017
                  SOURCE: C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/app/views/bookingViews/adminBookings.scala.html
                  HASH: 060747af1833c0062c352caf44cfce7ed4bc2b49
                  MATRIX: 664->23|693->47|1034->66|1159->96|1189->101|1231->135|1270->137|1300->141|5604->4417|5645->4441|5685->4442|5730->4458|5785->4485|5802->4492|5837->4505|5893->4533|5910->4540|5945->4553|6001->4581|6018->4588|6054->4602|6110->4630|6127->4637|6160->4648|6216->4676|6233->4683|6263->4691|6366->4766|6383->4773|6418->4786|6548->4888|6565->4895|6600->4908|6701->4977|6744->4991
                  LINES: 24->2|25->3|30->4|35->4|37->6|37->6|37->6|38->7|132->101|132->101|132->101|133->102|134->103|134->103|134->103|135->104|135->104|135->104|136->105|136->105|136->105|137->106|137->106|137->106|138->107|138->107|138->107|139->108|139->108|139->108|140->109|140->109|140->109|143->112|144->113
                  -- GENERATED --
              */
          