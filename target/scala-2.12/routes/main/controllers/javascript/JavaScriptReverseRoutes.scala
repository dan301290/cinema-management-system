
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/conf/routes
// @DATE:Thu Oct 12 15:14:05 BST 2017

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {

  // @LINE:59
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:59
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:33
  class ReverseAdminBookingController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:36
    def edit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminBookingController.edit",
      """
        function(bookingId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/bookings/edit/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("bookingId", bookingId0))})
        }
      """
    )
  
    // @LINE:34
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminBookingController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/bookings/create"})
        }
      """
    )
  
    // @LINE:33
    def bookings: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminBookingController.bookings",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/bookings"})
        }
      """
    )
  
    // @LINE:38
    def destroy: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminBookingController.destroy",
      """
        function(bookingId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/bookings/destroy/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("bookingId", bookingId0))})
        }
      """
    )
  
    // @LINE:35
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminBookingController.save",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/bookings/save"})
        }
      """
    )
  
    // @LINE:37
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminBookingController.update",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/bookings/update"})
        }
      """
    )
  
  }

  // @LINE:9
  class ReverseAdminController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def logout: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.logout",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
        }
      """
    )
  
    // @LINE:10
    def authenticate: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.authenticate",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "authenticate"})
        }
      """
    )
  
    // @LINE:11
    def admin: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.admin",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin"})
        }
      """
    )
  
    // @LINE:9
    def login: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.login",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "adminlogin"})
        }
      """
    )
  
  }

  // @LINE:49
  class ReverseAdminScreenController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:52
    def edit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminScreenController.edit",
      """
        function(screenId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/screens/edit/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("screenId", screenId0))})
        }
      """
    )
  
    // @LINE:50
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminScreenController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/screens/create"})
        }
      """
    )
  
    // @LINE:49
    def screens: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminScreenController.screens",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/screens"})
        }
      """
    )
  
    // @LINE:54
    def destroy: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminScreenController.destroy",
      """
        function(screenId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/screens/destroy/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("screenId", screenId0))})
        }
      """
    )
  
    // @LINE:51
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminScreenController.save",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/screens/save"})
        }
      """
    )
  
    // @LINE:53
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminScreenController.update",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/screens/update"})
        }
      """
    )
  
  }

  // @LINE:25
  class ReverseAdminShowingController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def edit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminShowingController.edit",
      """
        function(showingId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/showings/edit/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("showingId", showingId0))})
        }
      """
    )
  
    // @LINE:25
    def showings: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminShowingController.showings",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/showings"})
        }
      """
    )
  
    // @LINE:26
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminShowingController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/showings/create"})
        }
      """
    )
  
    // @LINE:30
    def destroy: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminShowingController.destroy",
      """
        function(showingId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/showings/destroy/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("showingId", showingId0))})
        }
      """
    )
  
    // @LINE:27
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminShowingController.save",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/showings/save"})
        }
      """
    )
  
    // @LINE:29
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminShowingController.update",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/showings/update"})
        }
      """
    )
  
  }

  // @LINE:41
  class ReverseAdminCustomerController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:44
    def edit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminCustomerController.edit",
      """
        function(customerId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/customers/edit/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("customerId", customerId0))})
        }
      """
    )
  
    // @LINE:42
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminCustomerController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/customers/create"})
        }
      """
    )
  
    // @LINE:41
    def customers: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminCustomerController.customers",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/customers"})
        }
      """
    )
  
    // @LINE:46
    def destroy: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminCustomerController.destroy",
      """
        function(customerId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/customers/destroy/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("customerId", customerId0))})
        }
      """
    )
  
    // @LINE:43
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminCustomerController.save",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/customers/save"})
        }
      """
    )
  
    // @LINE:45
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminCustomerController.update",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/customers/update"})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:16
  class ReverseAdminMovieController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def edit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminMovieController.edit",
      """
        function(movieId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/movies/edit/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("movieId", movieId0))})
        }
      """
    )
  
    // @LINE:17
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminMovieController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/movies/create"})
        }
      """
    )
  
    // @LINE:22
    def destroy: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminMovieController.destroy",
      """
        function(movieId0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/movies/destroy/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Integer]].javascriptUnbind + """)("movieId", movieId0))})
        }
      """
    )
  
    // @LINE:19
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminMovieController.save",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/movies/save"})
        }
      """
    )
  
    // @LINE:16
    def movies: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminMovieController.movies",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/movies"})
        }
      """
    )
  
    // @LINE:21
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminMovieController.update",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/movies/update"})
        }
      """
    )
  
  }


}
