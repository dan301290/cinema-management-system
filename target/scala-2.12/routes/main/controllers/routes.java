
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/conf/routes
// @DATE:Thu Oct 12 15:14:05 BST 2017

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAdminBookingController AdminBookingController = new controllers.ReverseAdminBookingController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAdminController AdminController = new controllers.ReverseAdminController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAdminScreenController AdminScreenController = new controllers.ReverseAdminScreenController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAdminShowingController AdminShowingController = new controllers.ReverseAdminShowingController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAdminCustomerController AdminCustomerController = new controllers.ReverseAdminCustomerController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseHomeController HomeController = new controllers.ReverseHomeController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAdminMovieController AdminMovieController = new controllers.ReverseAdminMovieController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAdminBookingController AdminBookingController = new controllers.javascript.ReverseAdminBookingController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAdminController AdminController = new controllers.javascript.ReverseAdminController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAdminScreenController AdminScreenController = new controllers.javascript.ReverseAdminScreenController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAdminShowingController AdminShowingController = new controllers.javascript.ReverseAdminShowingController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAdminCustomerController AdminCustomerController = new controllers.javascript.ReverseAdminCustomerController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseHomeController HomeController = new controllers.javascript.ReverseHomeController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAdminMovieController AdminMovieController = new controllers.javascript.ReverseAdminMovieController(RoutesPrefix.byNamePrefix());
  }

}
