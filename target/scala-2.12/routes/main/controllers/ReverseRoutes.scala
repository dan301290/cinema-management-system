
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/conf/routes
// @DATE:Thu Oct 12 15:14:05 BST 2017

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers {

  // @LINE:59
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:59
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:33
  class ReverseAdminBookingController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:36
    def edit(bookingId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/bookings/edit/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("bookingId", bookingId)))
    }
  
    // @LINE:34
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/bookings/create")
    }
  
    // @LINE:33
    def bookings(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/bookings")
    }
  
    // @LINE:38
    def destroy(bookingId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/bookings/destroy/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("bookingId", bookingId)))
    }
  
    // @LINE:35
    def save(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/bookings/save")
    }
  
    // @LINE:37
    def update(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/bookings/update")
    }
  
  }

  // @LINE:9
  class ReverseAdminController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def logout(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "logout")
    }
  
    // @LINE:10
    def authenticate(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "authenticate")
    }
  
    // @LINE:11
    def admin(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin")
    }
  
    // @LINE:9
    def login(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "adminlogin")
    }
  
  }

  // @LINE:49
  class ReverseAdminScreenController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:52
    def edit(screenId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/screens/edit/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("screenId", screenId)))
    }
  
    // @LINE:50
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/screens/create")
    }
  
    // @LINE:49
    def screens(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/screens")
    }
  
    // @LINE:54
    def destroy(screenId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/screens/destroy/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("screenId", screenId)))
    }
  
    // @LINE:51
    def save(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/screens/save")
    }
  
    // @LINE:53
    def update(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/screens/update")
    }
  
  }

  // @LINE:25
  class ReverseAdminShowingController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def edit(showingId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/showings/edit/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("showingId", showingId)))
    }
  
    // @LINE:25
    def showings(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/showings")
    }
  
    // @LINE:26
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/showings/create")
    }
  
    // @LINE:30
    def destroy(showingId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/showings/destroy/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("showingId", showingId)))
    }
  
    // @LINE:27
    def save(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/showings/save")
    }
  
    // @LINE:29
    def update(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/showings/update")
    }
  
  }

  // @LINE:41
  class ReverseAdminCustomerController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:44
    def edit(customerId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/customers/edit/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("customerId", customerId)))
    }
  
    // @LINE:42
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/customers/create")
    }
  
    // @LINE:41
    def customers(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/customers")
    }
  
    // @LINE:46
    def destroy(customerId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/customers/destroy/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("customerId", customerId)))
    }
  
    // @LINE:43
    def save(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/customers/save")
    }
  
    // @LINE:45
    def update(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/customers/update")
    }
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }

  // @LINE:16
  class ReverseAdminMovieController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def edit(movieId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/movies/edit/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("movieId", movieId)))
    }
  
    // @LINE:17
    def create(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/movies/create")
    }
  
    // @LINE:22
    def destroy(movieId:Integer): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/movies/destroy/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Integer]].unbind("movieId", movieId)))
    }
  
    // @LINE:19
    def save(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/movies/save")
    }
  
    // @LINE:16
    def movies(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/movies")
    }
  
    // @LINE:21
    def update(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "admin/movies/update")
    }
  
  }


}
