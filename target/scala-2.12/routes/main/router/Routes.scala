
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/conf/routes
// @DATE:Thu Oct 12 15:14:05 BST 2017

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_2: controllers.HomeController,
  // @LINE:9
  AdminController_0: controllers.AdminController,
  // @LINE:16
  AdminMovieController_4: controllers.AdminMovieController,
  // @LINE:25
  AdminShowingController_1: controllers.AdminShowingController,
  // @LINE:33
  AdminBookingController_3: controllers.AdminBookingController,
  // @LINE:41
  AdminCustomerController_5: controllers.AdminCustomerController,
  // @LINE:49
  AdminScreenController_6: controllers.AdminScreenController,
  // @LINE:59
  Assets_7: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_2: controllers.HomeController,
    // @LINE:9
    AdminController_0: controllers.AdminController,
    // @LINE:16
    AdminMovieController_4: controllers.AdminMovieController,
    // @LINE:25
    AdminShowingController_1: controllers.AdminShowingController,
    // @LINE:33
    AdminBookingController_3: controllers.AdminBookingController,
    // @LINE:41
    AdminCustomerController_5: controllers.AdminCustomerController,
    // @LINE:49
    AdminScreenController_6: controllers.AdminScreenController,
    // @LINE:59
    Assets_7: controllers.Assets
  ) = this(errorHandler, HomeController_2, AdminController_0, AdminMovieController_4, AdminShowingController_1, AdminBookingController_3, AdminCustomerController_5, AdminScreenController_6, Assets_7, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_2, AdminController_0, AdminMovieController_4, AdminShowingController_1, AdminBookingController_3, AdminCustomerController_5, AdminScreenController_6, Assets_7, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """adminlogin""", """controllers.AdminController.login"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """authenticate""", """controllers.AdminController.authenticate"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin""", """controllers.AdminController.admin"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logout""", """controllers.AdminController.logout"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/movies""", """controllers.AdminMovieController.movies"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/movies/create""", """controllers.AdminMovieController.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/movies/save""", """controllers.AdminMovieController.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/movies/edit/""" + "$" + """movieId<[^/]+>""", """controllers.AdminMovieController.edit(movieId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/movies/update""", """controllers.AdminMovieController.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/movies/destroy/""" + "$" + """movieId<[^/]+>""", """controllers.AdminMovieController.destroy(movieId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/showings""", """controllers.AdminShowingController.showings"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/showings/create""", """controllers.AdminShowingController.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/showings/save""", """controllers.AdminShowingController.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/showings/edit/""" + "$" + """showingId<[^/]+>""", """controllers.AdminShowingController.edit(showingId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/showings/update""", """controllers.AdminShowingController.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/showings/destroy/""" + "$" + """showingId<[^/]+>""", """controllers.AdminShowingController.destroy(showingId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/bookings""", """controllers.AdminBookingController.bookings"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/bookings/create""", """controllers.AdminBookingController.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/bookings/save""", """controllers.AdminBookingController.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/bookings/edit/""" + "$" + """bookingId<[^/]+>""", """controllers.AdminBookingController.edit(bookingId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/bookings/update""", """controllers.AdminBookingController.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/bookings/destroy/""" + "$" + """bookingId<[^/]+>""", """controllers.AdminBookingController.destroy(bookingId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/customers""", """controllers.AdminCustomerController.customers"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/customers/create""", """controllers.AdminCustomerController.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/customers/save""", """controllers.AdminCustomerController.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/customers/edit/""" + "$" + """customerId<[^/]+>""", """controllers.AdminCustomerController.edit(customerId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/customers/update""", """controllers.AdminCustomerController.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/customers/destroy/""" + "$" + """customerId<[^/]+>""", """controllers.AdminCustomerController.destroy(customerId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/screens""", """controllers.AdminScreenController.screens"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/screens/create""", """controllers.AdminScreenController.create"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/screens/save""", """controllers.AdminScreenController.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/screens/edit/""" + "$" + """screenId<[^/]+>""", """controllers.AdminScreenController.edit(screenId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/screens/update""", """controllers.AdminScreenController.update"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/screens/destroy/""" + "$" + """screenId<[^/]+>""", """controllers.AdminScreenController.destroy(screenId:Integer)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_2.index,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      this.prefix + """""",
      """ open example movie front-end""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_AdminController_login1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("adminlogin")))
  )
  private[this] lazy val controllers_AdminController_login1_invoker = createInvoker(
    AdminController_0.login,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "login",
      Nil,
      "GET",
      this.prefix + """adminlogin""",
      """open admin back-end""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_AdminController_authenticate2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("authenticate")))
  )
  private[this] lazy val controllers_AdminController_authenticate2_invoker = createInvoker(
    AdminController_0.authenticate,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "authenticate",
      Nil,
      "GET",
      this.prefix + """authenticate""",
      """""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_AdminController_admin3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin")))
  )
  private[this] lazy val controllers_AdminController_admin3_invoker = createInvoker(
    AdminController_0.admin,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "admin",
      Nil,
      "GET",
      this.prefix + """admin""",
      """""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_AdminController_logout4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logout")))
  )
  private[this] lazy val controllers_AdminController_logout4_invoker = createInvoker(
    AdminController_0.logout,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "logout",
      Nil,
      "GET",
      this.prefix + """logout""",
      """""",
      Seq()
    )
  )

  // @LINE:16
  private[this] lazy val controllers_AdminMovieController_movies5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/movies")))
  )
  private[this] lazy val controllers_AdminMovieController_movies5_invoker = createInvoker(
    AdminMovieController_4.movies,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminMovieController",
      "movies",
      Nil,
      "GET",
      this.prefix + """admin/movies""",
      """ Movies, Add, Update and Delete""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_AdminMovieController_create6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/movies/create")))
  )
  private[this] lazy val controllers_AdminMovieController_create6_invoker = createInvoker(
    AdminMovieController_4.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminMovieController",
      "create",
      Nil,
      "GET",
      this.prefix + """admin/movies/create""",
      """""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_AdminMovieController_save7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/movies/save")))
  )
  private[this] lazy val controllers_AdminMovieController_save7_invoker = createInvoker(
    AdminMovieController_4.save,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminMovieController",
      "save",
      Nil,
      "GET",
      this.prefix + """admin/movies/save""",
      """GET     /admin/movies/create/lookup/:movieName              controllers.AdminMovieController.lookup(movieName: String)""",
      Seq()
    )
  )

  // @LINE:20
  private[this] lazy val controllers_AdminMovieController_edit8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/movies/edit/"), DynamicPart("movieId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminMovieController_edit8_invoker = createInvoker(
    AdminMovieController_4.edit(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminMovieController",
      "edit",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/movies/edit/""" + "$" + """movieId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:21
  private[this] lazy val controllers_AdminMovieController_update9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/movies/update")))
  )
  private[this] lazy val controllers_AdminMovieController_update9_invoker = createInvoker(
    AdminMovieController_4.update,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminMovieController",
      "update",
      Nil,
      "GET",
      this.prefix + """admin/movies/update""",
      """""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_AdminMovieController_destroy10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/movies/destroy/"), DynamicPart("movieId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminMovieController_destroy10_invoker = createInvoker(
    AdminMovieController_4.destroy(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminMovieController",
      "destroy",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/movies/destroy/""" + "$" + """movieId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:25
  private[this] lazy val controllers_AdminShowingController_showings11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/showings")))
  )
  private[this] lazy val controllers_AdminShowingController_showings11_invoker = createInvoker(
    AdminShowingController_1.showings,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminShowingController",
      "showings",
      Nil,
      "GET",
      this.prefix + """admin/showings""",
      """ Showings, Add, Update and Delete""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_AdminShowingController_create12_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/showings/create")))
  )
  private[this] lazy val controllers_AdminShowingController_create12_invoker = createInvoker(
    AdminShowingController_1.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminShowingController",
      "create",
      Nil,
      "GET",
      this.prefix + """admin/showings/create""",
      """""",
      Seq()
    )
  )

  // @LINE:27
  private[this] lazy val controllers_AdminShowingController_save13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/showings/save")))
  )
  private[this] lazy val controllers_AdminShowingController_save13_invoker = createInvoker(
    AdminShowingController_1.save,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminShowingController",
      "save",
      Nil,
      "GET",
      this.prefix + """admin/showings/save""",
      """""",
      Seq()
    )
  )

  // @LINE:28
  private[this] lazy val controllers_AdminShowingController_edit14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/showings/edit/"), DynamicPart("showingId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminShowingController_edit14_invoker = createInvoker(
    AdminShowingController_1.edit(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminShowingController",
      "edit",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/showings/edit/""" + "$" + """showingId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:29
  private[this] lazy val controllers_AdminShowingController_update15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/showings/update")))
  )
  private[this] lazy val controllers_AdminShowingController_update15_invoker = createInvoker(
    AdminShowingController_1.update,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminShowingController",
      "update",
      Nil,
      "GET",
      this.prefix + """admin/showings/update""",
      """""",
      Seq()
    )
  )

  // @LINE:30
  private[this] lazy val controllers_AdminShowingController_destroy16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/showings/destroy/"), DynamicPart("showingId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminShowingController_destroy16_invoker = createInvoker(
    AdminShowingController_1.destroy(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminShowingController",
      "destroy",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/showings/destroy/""" + "$" + """showingId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:33
  private[this] lazy val controllers_AdminBookingController_bookings17_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/bookings")))
  )
  private[this] lazy val controllers_AdminBookingController_bookings17_invoker = createInvoker(
    AdminBookingController_3.bookings,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminBookingController",
      "bookings",
      Nil,
      "GET",
      this.prefix + """admin/bookings""",
      """ Bookings, Add, Update and Delete""",
      Seq()
    )
  )

  // @LINE:34
  private[this] lazy val controllers_AdminBookingController_create18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/bookings/create")))
  )
  private[this] lazy val controllers_AdminBookingController_create18_invoker = createInvoker(
    AdminBookingController_3.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminBookingController",
      "create",
      Nil,
      "GET",
      this.prefix + """admin/bookings/create""",
      """""",
      Seq()
    )
  )

  // @LINE:35
  private[this] lazy val controllers_AdminBookingController_save19_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/bookings/save")))
  )
  private[this] lazy val controllers_AdminBookingController_save19_invoker = createInvoker(
    AdminBookingController_3.save,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminBookingController",
      "save",
      Nil,
      "GET",
      this.prefix + """admin/bookings/save""",
      """""",
      Seq()
    )
  )

  // @LINE:36
  private[this] lazy val controllers_AdminBookingController_edit20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/bookings/edit/"), DynamicPart("bookingId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminBookingController_edit20_invoker = createInvoker(
    AdminBookingController_3.edit(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminBookingController",
      "edit",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/bookings/edit/""" + "$" + """bookingId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:37
  private[this] lazy val controllers_AdminBookingController_update21_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/bookings/update")))
  )
  private[this] lazy val controllers_AdminBookingController_update21_invoker = createInvoker(
    AdminBookingController_3.update,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminBookingController",
      "update",
      Nil,
      "GET",
      this.prefix + """admin/bookings/update""",
      """""",
      Seq()
    )
  )

  // @LINE:38
  private[this] lazy val controllers_AdminBookingController_destroy22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/bookings/destroy/"), DynamicPart("bookingId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminBookingController_destroy22_invoker = createInvoker(
    AdminBookingController_3.destroy(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminBookingController",
      "destroy",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/bookings/destroy/""" + "$" + """bookingId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:41
  private[this] lazy val controllers_AdminCustomerController_customers23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/customers")))
  )
  private[this] lazy val controllers_AdminCustomerController_customers23_invoker = createInvoker(
    AdminCustomerController_5.customers,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminCustomerController",
      "customers",
      Nil,
      "GET",
      this.prefix + """admin/customers""",
      """ Customers, Add, Update and Delete""",
      Seq()
    )
  )

  // @LINE:42
  private[this] lazy val controllers_AdminCustomerController_create24_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/customers/create")))
  )
  private[this] lazy val controllers_AdminCustomerController_create24_invoker = createInvoker(
    AdminCustomerController_5.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminCustomerController",
      "create",
      Nil,
      "GET",
      this.prefix + """admin/customers/create""",
      """""",
      Seq()
    )
  )

  // @LINE:43
  private[this] lazy val controllers_AdminCustomerController_save25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/customers/save")))
  )
  private[this] lazy val controllers_AdminCustomerController_save25_invoker = createInvoker(
    AdminCustomerController_5.save,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminCustomerController",
      "save",
      Nil,
      "GET",
      this.prefix + """admin/customers/save""",
      """""",
      Seq()
    )
  )

  // @LINE:44
  private[this] lazy val controllers_AdminCustomerController_edit26_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/customers/edit/"), DynamicPart("customerId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminCustomerController_edit26_invoker = createInvoker(
    AdminCustomerController_5.edit(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminCustomerController",
      "edit",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/customers/edit/""" + "$" + """customerId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:45
  private[this] lazy val controllers_AdminCustomerController_update27_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/customers/update")))
  )
  private[this] lazy val controllers_AdminCustomerController_update27_invoker = createInvoker(
    AdminCustomerController_5.update,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminCustomerController",
      "update",
      Nil,
      "GET",
      this.prefix + """admin/customers/update""",
      """""",
      Seq()
    )
  )

  // @LINE:46
  private[this] lazy val controllers_AdminCustomerController_destroy28_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/customers/destroy/"), DynamicPart("customerId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminCustomerController_destroy28_invoker = createInvoker(
    AdminCustomerController_5.destroy(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminCustomerController",
      "destroy",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/customers/destroy/""" + "$" + """customerId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:49
  private[this] lazy val controllers_AdminScreenController_screens29_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/screens")))
  )
  private[this] lazy val controllers_AdminScreenController_screens29_invoker = createInvoker(
    AdminScreenController_6.screens,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminScreenController",
      "screens",
      Nil,
      "GET",
      this.prefix + """admin/screens""",
      """ Screens, Add, Update and Delete""",
      Seq()
    )
  )

  // @LINE:50
  private[this] lazy val controllers_AdminScreenController_create30_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/screens/create")))
  )
  private[this] lazy val controllers_AdminScreenController_create30_invoker = createInvoker(
    AdminScreenController_6.create,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminScreenController",
      "create",
      Nil,
      "GET",
      this.prefix + """admin/screens/create""",
      """""",
      Seq()
    )
  )

  // @LINE:51
  private[this] lazy val controllers_AdminScreenController_save31_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/screens/save")))
  )
  private[this] lazy val controllers_AdminScreenController_save31_invoker = createInvoker(
    AdminScreenController_6.save,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminScreenController",
      "save",
      Nil,
      "GET",
      this.prefix + """admin/screens/save""",
      """""",
      Seq()
    )
  )

  // @LINE:52
  private[this] lazy val controllers_AdminScreenController_edit32_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/screens/edit/"), DynamicPart("screenId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminScreenController_edit32_invoker = createInvoker(
    AdminScreenController_6.edit(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminScreenController",
      "edit",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/screens/edit/""" + "$" + """screenId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:53
  private[this] lazy val controllers_AdminScreenController_update33_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/screens/update")))
  )
  private[this] lazy val controllers_AdminScreenController_update33_invoker = createInvoker(
    AdminScreenController_6.update,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminScreenController",
      "update",
      Nil,
      "GET",
      this.prefix + """admin/screens/update""",
      """""",
      Seq()
    )
  )

  // @LINE:54
  private[this] lazy val controllers_AdminScreenController_destroy34_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/screens/destroy/"), DynamicPart("screenId", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminScreenController_destroy34_invoker = createInvoker(
    AdminScreenController_6.destroy(fakeValue[Integer]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminScreenController",
      "destroy",
      Seq(classOf[Integer]),
      "GET",
      this.prefix + """admin/screens/destroy/""" + "$" + """screenId<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:59
  private[this] lazy val controllers_Assets_versioned35_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned35_invoker = createInvoker(
    Assets_7.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params@_) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_2.index)
      }
  
    // @LINE:9
    case controllers_AdminController_login1_route(params@_) =>
      call { 
        controllers_AdminController_login1_invoker.call(AdminController_0.login)
      }
  
    // @LINE:10
    case controllers_AdminController_authenticate2_route(params@_) =>
      call { 
        controllers_AdminController_authenticate2_invoker.call(AdminController_0.authenticate)
      }
  
    // @LINE:11
    case controllers_AdminController_admin3_route(params@_) =>
      call { 
        controllers_AdminController_admin3_invoker.call(AdminController_0.admin)
      }
  
    // @LINE:12
    case controllers_AdminController_logout4_route(params@_) =>
      call { 
        controllers_AdminController_logout4_invoker.call(AdminController_0.logout)
      }
  
    // @LINE:16
    case controllers_AdminMovieController_movies5_route(params@_) =>
      call { 
        controllers_AdminMovieController_movies5_invoker.call(AdminMovieController_4.movies)
      }
  
    // @LINE:17
    case controllers_AdminMovieController_create6_route(params@_) =>
      call { 
        controllers_AdminMovieController_create6_invoker.call(AdminMovieController_4.create)
      }
  
    // @LINE:19
    case controllers_AdminMovieController_save7_route(params@_) =>
      call { 
        controllers_AdminMovieController_save7_invoker.call(AdminMovieController_4.save)
      }
  
    // @LINE:20
    case controllers_AdminMovieController_edit8_route(params@_) =>
      call(params.fromPath[Integer]("movieId", None)) { (movieId) =>
        controllers_AdminMovieController_edit8_invoker.call(AdminMovieController_4.edit(movieId))
      }
  
    // @LINE:21
    case controllers_AdminMovieController_update9_route(params@_) =>
      call { 
        controllers_AdminMovieController_update9_invoker.call(AdminMovieController_4.update)
      }
  
    // @LINE:22
    case controllers_AdminMovieController_destroy10_route(params@_) =>
      call(params.fromPath[Integer]("movieId", None)) { (movieId) =>
        controllers_AdminMovieController_destroy10_invoker.call(AdminMovieController_4.destroy(movieId))
      }
  
    // @LINE:25
    case controllers_AdminShowingController_showings11_route(params@_) =>
      call { 
        controllers_AdminShowingController_showings11_invoker.call(AdminShowingController_1.showings)
      }
  
    // @LINE:26
    case controllers_AdminShowingController_create12_route(params@_) =>
      call { 
        controllers_AdminShowingController_create12_invoker.call(AdminShowingController_1.create)
      }
  
    // @LINE:27
    case controllers_AdminShowingController_save13_route(params@_) =>
      call { 
        controllers_AdminShowingController_save13_invoker.call(AdminShowingController_1.save)
      }
  
    // @LINE:28
    case controllers_AdminShowingController_edit14_route(params@_) =>
      call(params.fromPath[Integer]("showingId", None)) { (showingId) =>
        controllers_AdminShowingController_edit14_invoker.call(AdminShowingController_1.edit(showingId))
      }
  
    // @LINE:29
    case controllers_AdminShowingController_update15_route(params@_) =>
      call { 
        controllers_AdminShowingController_update15_invoker.call(AdminShowingController_1.update)
      }
  
    // @LINE:30
    case controllers_AdminShowingController_destroy16_route(params@_) =>
      call(params.fromPath[Integer]("showingId", None)) { (showingId) =>
        controllers_AdminShowingController_destroy16_invoker.call(AdminShowingController_1.destroy(showingId))
      }
  
    // @LINE:33
    case controllers_AdminBookingController_bookings17_route(params@_) =>
      call { 
        controllers_AdminBookingController_bookings17_invoker.call(AdminBookingController_3.bookings)
      }
  
    // @LINE:34
    case controllers_AdminBookingController_create18_route(params@_) =>
      call { 
        controllers_AdminBookingController_create18_invoker.call(AdminBookingController_3.create)
      }
  
    // @LINE:35
    case controllers_AdminBookingController_save19_route(params@_) =>
      call { 
        controllers_AdminBookingController_save19_invoker.call(AdminBookingController_3.save)
      }
  
    // @LINE:36
    case controllers_AdminBookingController_edit20_route(params@_) =>
      call(params.fromPath[Integer]("bookingId", None)) { (bookingId) =>
        controllers_AdminBookingController_edit20_invoker.call(AdminBookingController_3.edit(bookingId))
      }
  
    // @LINE:37
    case controllers_AdminBookingController_update21_route(params@_) =>
      call { 
        controllers_AdminBookingController_update21_invoker.call(AdminBookingController_3.update)
      }
  
    // @LINE:38
    case controllers_AdminBookingController_destroy22_route(params@_) =>
      call(params.fromPath[Integer]("bookingId", None)) { (bookingId) =>
        controllers_AdminBookingController_destroy22_invoker.call(AdminBookingController_3.destroy(bookingId))
      }
  
    // @LINE:41
    case controllers_AdminCustomerController_customers23_route(params@_) =>
      call { 
        controllers_AdminCustomerController_customers23_invoker.call(AdminCustomerController_5.customers)
      }
  
    // @LINE:42
    case controllers_AdminCustomerController_create24_route(params@_) =>
      call { 
        controllers_AdminCustomerController_create24_invoker.call(AdminCustomerController_5.create)
      }
  
    // @LINE:43
    case controllers_AdminCustomerController_save25_route(params@_) =>
      call { 
        controllers_AdminCustomerController_save25_invoker.call(AdminCustomerController_5.save)
      }
  
    // @LINE:44
    case controllers_AdminCustomerController_edit26_route(params@_) =>
      call(params.fromPath[Integer]("customerId", None)) { (customerId) =>
        controllers_AdminCustomerController_edit26_invoker.call(AdminCustomerController_5.edit(customerId))
      }
  
    // @LINE:45
    case controllers_AdminCustomerController_update27_route(params@_) =>
      call { 
        controllers_AdminCustomerController_update27_invoker.call(AdminCustomerController_5.update)
      }
  
    // @LINE:46
    case controllers_AdminCustomerController_destroy28_route(params@_) =>
      call(params.fromPath[Integer]("customerId", None)) { (customerId) =>
        controllers_AdminCustomerController_destroy28_invoker.call(AdminCustomerController_5.destroy(customerId))
      }
  
    // @LINE:49
    case controllers_AdminScreenController_screens29_route(params@_) =>
      call { 
        controllers_AdminScreenController_screens29_invoker.call(AdminScreenController_6.screens)
      }
  
    // @LINE:50
    case controllers_AdminScreenController_create30_route(params@_) =>
      call { 
        controllers_AdminScreenController_create30_invoker.call(AdminScreenController_6.create)
      }
  
    // @LINE:51
    case controllers_AdminScreenController_save31_route(params@_) =>
      call { 
        controllers_AdminScreenController_save31_invoker.call(AdminScreenController_6.save)
      }
  
    // @LINE:52
    case controllers_AdminScreenController_edit32_route(params@_) =>
      call(params.fromPath[Integer]("screenId", None)) { (screenId) =>
        controllers_AdminScreenController_edit32_invoker.call(AdminScreenController_6.edit(screenId))
      }
  
    // @LINE:53
    case controllers_AdminScreenController_update33_route(params@_) =>
      call { 
        controllers_AdminScreenController_update33_invoker.call(AdminScreenController_6.update)
      }
  
    // @LINE:54
    case controllers_AdminScreenController_destroy34_route(params@_) =>
      call(params.fromPath[Integer]("screenId", None)) { (screenId) =>
        controllers_AdminScreenController_destroy34_invoker.call(AdminScreenController_6.destroy(screenId))
      }
  
    // @LINE:59
    case controllers_Assets_versioned35_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned35_invoker.call(Assets_7.versioned(path, file))
      }
  }
}
