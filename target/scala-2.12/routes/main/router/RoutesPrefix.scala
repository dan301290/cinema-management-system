
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/dan30/Dropbox/Apps/Heroku/cinema-management-system/conf/routes
// @DATE:Thu Oct 12 15:14:05 BST 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
