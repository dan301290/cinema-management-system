package services;

import javax.inject.*;

import models.User;
import models.Movie;
import play.db.*;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * This service handles the creation and management of the database.
 */

public class DBHandler {

    public DBHandler(){

        // setup movies
        Movie movie1 = new Movie(1, "Blade Runner 2049", "15 - Suitable only for 15 years and over", "Blade Runner 2049 is a 2017 American neo-noir science fiction film directed by Denis Villeneuve and written by Hampton Fancher and Michael Green.", "A young blade runner's discovery of a long-buried secret leads him to track down former blade runner Rick Deckard, who's been missing for thirty years", "http://cdn1.sciencefiction.com/wp-content/uploads/2017/06/blade-runner-2049.jpg", "2017", "Sci-Fi, Thriller", "06 Oct 2017", "Denis Villeneuve", "163 min");
        Movie movie2 = new Movie(2, "Kingsman: The Golden Circle", "18 - Suitable only for adults", "Kingsman: The Golden Circle is a 2017 action spy comedy film produced and directed by Matthew Vaughn and written by Vaughn and Jane Goldman.", "When their headquarters are destroyed and the world is held hostage, the Kingsman's journey leads them to the discovery of an allied spy organization in the US. These two elite secret organizations must band together to defeat a common enemy", "http://gstylemag-zippykid.netdna-ssl.com/wp-content/uploads/2017/09/TheKingsmanTheGoldenCircleBanner.jpg", "2017", "Action, Adventure, Comedy", "22 Sep 2017", "Matthew Vaughn", "141 min");
        Movie movie3 = new Movie(3, "The Lego Ninjago movie", "PG - Parental guidance", "The Lego Ninjago Movie is a 2017 3D computer-animated action comedy martial arts film produced by Warner Animation Group. Co-directed by Charlie Bean, ...", "Shunned by everyone for being the son of an evil warlord, a teenager seeks to defeat him with the help of his fellow ninjas.", "http://whysoblu.com/wp-content/uploads/2017/09/lego-ninjago-movie-banner-2.png", "2017", "Animation, Action, Adventure", "22 Sep 2017", "Charlie Bean, Paul Fisher, Bob Logan", "101 min");
        if (Movie.find.byId(1) == null){
            movie1.save();
            movie2.save();
            movie3.save();
        }

        // setup users
        User user1 = new User(1, "Admin", "Admin");
        if (User.find.byId(1) == null){
            user1.save();
        }
    }
}
