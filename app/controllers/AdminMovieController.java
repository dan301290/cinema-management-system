package controllers;

import models.Movie;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.omdbApi;

import javax.inject.Inject;
import java.util.List;

public class AdminMovieController extends Controller{

    omdbApi movieSearch = new omdbApi();


    @Inject
    FormFactory formFactory;

    // Movies dashboard
    @Security.Authenticated(Secured.class)
    public Result movies() {
        List<Movie> movies = Movie.find.all();
        return ok(views.html.movieViews.adminMovies.render(movies));
    }

    // Form for adding new movie
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Movie> movieForm = formFactory.form(Movie.class);
        return ok(views.html.movieViews.adminMovieAdd.render( movieForm));
    }

    // Save result from add new movie form
    @Security.Authenticated(Secured.class)
    public Result save() {
        Form<Movie> movieForm = formFactory.form(Movie.class).bindFromRequest();
        Movie movie = movieForm.get();
        movie.save();
        return redirect(routes.AdminMovieController.movies());
    }

    // Form for editing a movie
    @Security.Authenticated(Secured.class)
    public Result edit(Integer movieId) {
        Movie movie = Movie.find.byId(movieId);
        if(movie==null){
            return notFound("Movie Not found");
        }
        Form<Movie> movieForm = formFactory.form(Movie.class).fill(movie);
        return ok(views.html.movieViews.adminMovieEdit.render(movieForm));
    }

    // Save result from form update
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Movie> movieForm = formFactory.form(Movie.class).bindFromRequest();
        Movie movie = movieForm.get();
        Movie oldMovie = Movie.find.byId(movie.getMovieId());
        if (oldMovie == null){
            return notFound("Movie Not Found");
        }

        oldMovie.setMovieName(movie.getMovieName());
        oldMovie.setAgeRating(movie.getAgeRating());
        oldMovie.setShortDesc(movie.getShortDesc());
        oldMovie.setPlot(movie.getPlot());
        oldMovie.setImg_URL(movie.getImg_URL());

        oldMovie.setYear(movie.getYear());
        oldMovie.setGenre(movie.getGenre());
        oldMovie.setReleased(movie.getReleased());
        oldMovie.setDirector(movie.getDirector());
        oldMovie.setRuntime(movie.getRuntime());

        oldMovie.update();


        List<Movie> movies = Movie.find.all();
        return redirect(routes.AdminMovieController.movies());
    }


    @Security.Authenticated(Secured.class)
    public Result show() {
        List<Movie> movies = Movie.find.all();
        return ok(views.html.movieViews.adminMovies.render(movies ));
    }

    // Delete a movie
    @Security.Authenticated(Secured.class)
    public Result destroy(Integer movieId) {
        Movie movie = Movie.find.byId(movieId);
        if(movie==null){
            return notFound("Movie Not found");
        }

        movie.delete();
        return redirect(routes.AdminMovieController.movies());
    }

}
