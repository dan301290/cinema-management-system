package controllers;

import play.mvc.*;
import models.Movie;
import services.*;

import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    DBHandler DBSetup = new DBHandler();

    public Result index() {
        List<Movie> movies = Movie.find.all();
        return ok(views.html.index.render(movies));
    }

}
