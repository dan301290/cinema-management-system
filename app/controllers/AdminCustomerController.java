package controllers;

import models.Customer;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import javax.inject.Inject;
import java.util.List;

public class AdminCustomerController extends Controller {

    @Inject
    FormFactory formFactory;

    // Movies dashboard
    @Security.Authenticated(Secured.class)
    public Result customers() {
        List<Customer> customers = Customer.find.all();
        return ok(views.html.customerViews.adminCustomers.render(customers));
    }

    // Form for adding new customer
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Customer> customerForm = formFactory.form(Customer.class);
        return ok(views.html.customerViews.adminCustomerAdd.render(customerForm));
    }

    // Save result from add new customer form
    @Security.Authenticated(Secured.class)
    public Result save() {
        Form<Customer> customerForm = formFactory.form(Customer.class).bindFromRequest();
        Customer customer = customerForm.get();
        customer.save();
        return redirect(routes.AdminCustomerController.customers());
    }

    // Form for editing a customer
    @Security.Authenticated(Secured.class)
    public Result edit(Integer customerId) {
        Customer customer = Customer.find.byId(customerId);
        if(customer==null){
            return notFound("Customer Not found");
        }
        Form<Customer> customerForm = formFactory.form(Customer.class).fill(customer);
        return ok(views.html.customerViews.adminCustomerEdit.render(customerForm));
    }

    // Save result from form update
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Customer> customerForm = formFactory.form(Customer.class).bindFromRequest();
        Customer customer = customerForm.get();
        Customer oldCustomer = Customer.find.byId(customer.getCustomerId());
        if (oldCustomer == null){
            return notFound("Customer Not Found");
        }

        oldCustomer.setFirstName(customer.getFirstName());
        oldCustomer.setLastName(customer.getLastName());
        oldCustomer.setEmail(customer.getEmail());
        oldCustomer.setPhone(customer.getPhone());
        oldCustomer.setAddress(customer.getAddress());

        oldCustomer.update();


        List<Customer> customers = Customer.find.all();
        return redirect(routes.AdminCustomerController.customers());
    }


    @Security.Authenticated(Secured.class)
    public Result show() {
        List<Customer> customers = Customer.find.all();
        return ok(views.html.customerViews.adminCustomers.render(customers));
    }

    // Delete a customer
    @Security.Authenticated(Secured.class)
    public Result destroy(Integer customerId) {
        Customer customer = Customer.find.byId(customerId);
        if(customer==null){
            return notFound("Customer Not found");
        }

        customer.delete();
        return redirect(routes.AdminCustomerController.customers());
    }

}
