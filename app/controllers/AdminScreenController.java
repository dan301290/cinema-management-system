package controllers;

import models.Screen;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import javax.inject.Inject;
import java.util.List;

public class AdminScreenController extends Controller {

    @Inject
    FormFactory formFactory;

    // Movies dashboard
    @Security.Authenticated(Secured.class)
    public Result screens() {
        List<Screen> screens = Screen.find.all();
        return ok(views.html.screenViews.adminScreens.render(screens));
    }

    // Form for adding new screen
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Screen> screenForm = formFactory.form(Screen.class);
        return ok(views.html.screenViews.adminScreenAdd.render(screenForm));
    }

    // Save result from add new screen form
    @Security.Authenticated(Secured.class)
    public Result save() {
        Form<Screen> screenForm = formFactory.form(Screen.class).bindFromRequest();
        Screen screen = screenForm.get();
        screen.save();
        return redirect(routes.AdminScreenController.screens());
    }

    // Form for editing a screen
    @Security.Authenticated(Secured.class)
    public Result edit(Integer screenId) {
        Screen screen = Screen.find.byId(screenId);
        if(screen==null){
            return notFound("Screen Not found");
        }
        Form<Screen> screenForm = formFactory.form(Screen.class).fill(screen);
        return ok(views.html.screenViews.adminScreenEdit.render(screenForm));
    }

    // Save result from form update
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Screen> screenForm = formFactory.form(Screen.class).bindFromRequest();
        Screen screen = screenForm.get();
        Screen oldScreen = Screen.find.byId(screen.getScreenId());
        if (oldScreen == null){
            return notFound("Screen Not Found");
        }

        oldScreen.setTotalSeats(screen.getTotalSeats());
        oldScreen.setScreenName(screen.getScreenName());

        oldScreen.update();


        List<Screen> screens = Screen.find.all();
        return redirect(routes.AdminScreenController.screens());
    }


    @Security.Authenticated(Secured.class)
    public Result show() {
        List<Screen> screens = Screen.find.all();
        return ok(views.html.screenViews.adminScreens.render(screens));
    }

    // Delete a screen
    @Security.Authenticated(Secured.class)
    public Result destroy(Integer screenId) {
        Screen screen = Screen.find.byId(screenId);
        if(screen==null){
            return notFound("Screen Not found");
        }

        screen.delete();
        return redirect(routes.AdminScreenController.screens());
    }
    
}
