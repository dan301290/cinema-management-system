package controllers;

import models.Booking;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import javax.inject.Inject;
import java.util.List;

public class AdminBookingController extends Controller {

    @Inject
    FormFactory formFactory;

    // Bookings dashboard
    @Security.Authenticated(Secured.class)
    public Result bookings() {
        List<Booking> bookings = Booking.find.all();
        return ok(views.html.bookingViews.adminBookings.render(bookings));
    }

    // Form for adding new booking
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Booking> movieForm = formFactory.form(Booking.class);
        return ok(views.html.bookingViews.adminBookingAdd.render( movieForm));
    }

    // Save result from add new booking form
    @Security.Authenticated(Secured.class)
    public Result save() {
        Form<Booking> movieForm = formFactory.form(Booking.class).bindFromRequest();
        Booking booking = movieForm.get();
        booking.save();
        return redirect(routes.AdminBookingController.bookings());
    }

    // Form for editing a booking
    @Security.Authenticated(Secured.class)
    public Result edit(Integer movieId) {
        Booking booking = Booking.find.byId(movieId);
        if(booking==null){
            return notFound("Booking Not found");
        }
        Form<Booking> movieForm = formFactory.form(Booking.class).fill(booking);
        return ok(views.html.bookingViews.adminBookingEdit.render(movieForm));
    }

    // Save result from form update
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Booking> movieForm = formFactory.form(Booking.class).bindFromRequest();
        Booking booking = movieForm.get();
        Booking oldBooking = Booking.find.byId(booking.getBookingId());
        if (oldBooking == null){
            return notFound("Booking Not Found");
        }

        oldBooking.setShowingId(booking.getShowingId());
        oldBooking.setCustomerId(booking.getCustomerId());
        oldBooking.setSeatNum(booking.getSeatNum());
        oldBooking.setDate(booking.getDate());

        oldBooking.update();


        List<Booking> bookings = Booking.find.all();
        return redirect(routes.AdminBookingController.bookings());
    }


    @Security.Authenticated(Secured.class)
    public Result show() {
        List<Booking> bookings = Booking.find.all();
        return ok(views.html.bookingViews.adminBookings.render(bookings));
    }

    // Delete a booking
    @Security.Authenticated(Secured.class)
    public Result destroy(Integer movieId) {
        Booking booking = Booking.find.byId(movieId);
        if(booking==null){
            return notFound("Booking Not found");
        }

        booking.delete();
        return redirect(routes.AdminBookingController.bookings());
    }
    
}
