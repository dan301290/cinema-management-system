package controllers;

import io.ebean.Finder;
import models.*;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;
import services.*;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

public class AdminController extends Controller{

    DBHandler DBSetup = new DBHandler();

    @Inject
    FormFactory formFactory;

    public Result login() {
        Form<User> loginForm = formFactory.form(User.class);
        return ok(views.html.login.render(loginForm));
    }

    public Result authenticate() {
        Form<User> loginForm = formFactory.form(User.class).bindFromRequest();
        User user = loginForm.get();
        // Query database to authenticate user exists.
        int count = User.find.query().where()
                .like("userName", user.getUserName())
                .like("password", user.getPassword()).findCount();
        if ( count <= 0) {

            flash("danger", "Incorrect Login - A user with those details does not exist");

            return badRequest(views.html.login.render(loginForm));
        } else {
            session().clear();
            session("userName", user.getUserName());

            long currentT = new Date().getTime();
            String currentTs = String.valueOf(currentT);
            session("userTime", currentTs);
            return redirect(routes.AdminController.admin());
        }
    }

    @Security.Authenticated(Secured.class)
    public Result admin() {
        return ok(views.html.adminIndex.render());
    }

    public Result logout() {
        session().clear();
        return redirect(routes.AdminController.login());
    }


}
