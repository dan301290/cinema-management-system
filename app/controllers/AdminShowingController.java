package controllers;

import models.Showing;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import javax.inject.Inject;
import java.util.List;

public class AdminShowingController extends Controller{

    @Inject
    FormFactory formFactory;

    // Showings dashboard
    @Security.Authenticated(Secured.class)
    public Result showings() {
        List<Showing> showings = Showing.find.all();
        return ok(views.html.showingViews.adminShowings.render(showings));
    }

    // Form for adding new showing
    @Security.Authenticated(Secured.class)
    public Result create() {
        Form<Showing> showingForm = formFactory.form(Showing.class);
        return ok(views.html.showingViews.adminShowingAdd.render(showingForm));
    }

    // Save result from add new showing form
    @Security.Authenticated(Secured.class)
    public Result save() {
        Form<Showing> showingForm = formFactory.form(Showing.class).bindFromRequest();
        Showing showing = showingForm.get();
        showing.save();
        return redirect(routes.AdminShowingController.showings());
    }

    // Form for editing a showing
    @Security.Authenticated(Secured.class)
    public Result edit(Integer showingId) {
        Showing showing = Showing.find.byId(showingId);
        if(showing==null){
            return notFound("Showing Not found");
        }
        Form<Showing> showingForm = formFactory.form(Showing.class).fill(showing);
        return ok(views.html.showingViews.adminShowingEdit.render(showingForm));
    }

    // Save result from form update
    @Security.Authenticated(Secured.class)
    public Result update() {
        Form<Showing> showingForm = formFactory.form(Showing.class).bindFromRequest();
        Showing showing = showingForm.get();
        Showing oldShowing = Showing.find.byId(showing.getShowingId());
        if (oldShowing == null){
            return notFound("Showing Not Found");
        }

        oldShowing.setScreenId(showing.getScreenId());
        oldShowing.setStartTime(showing.getStartTime());
        oldShowing.setEndTime(showing.getEndTime());
        oldShowing.setDate(showing.getDate());

        oldShowing.update();


        List<Showing> showings = Showing.find.all();
        return redirect(routes.AdminShowingController.showings());
    }


    @Security.Authenticated(Secured.class)
    public Result show() {
        List<Showing> showings = Showing.find.all();
        return ok(views.html.showingViews.adminShowings.render(showings));
    }

    // Delete a showing
    @Security.Authenticated(Secured.class)
    public Result destroy(Integer showingId) {
        Showing showing = Showing.find.byId(showingId);
        if(showing==null){
            return notFound("Showing Not found");
        }

        showing.delete();
        return redirect(routes.AdminShowingController.showings());
    }

}
