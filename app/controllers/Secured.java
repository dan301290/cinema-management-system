package controllers;

import models.User;
import play.Play;
import play.mvc.Http.*;
import play.mvc.Result;
import play.mvc.Security;
import java.util.Date;

import static play.mvc.Controller.flash;
import static play.mvc.Controller.session;

public class Secured extends Security.Authenticator {

    @Override
    public String getUsername(Context ctx)
    {
        // see if user is logged in
        if (session("userName") == null)
            return null;


        // see if the session is expired
        String previousTick = session("userTime");
        if (previousTick != null && !previousTick.equals("")) {
            long previousT = Long.valueOf(previousTick);
            long currentT = new Date().getTime();
            long timeout = Long.valueOf(Play.application().configuration().getString("sessionTimeout")) * 1000 * 60;
            if ((currentT - previousT) > timeout) {
                // session expired
                session().clear();
                return null;
            }
        }

        // update time in session
        String tickString = Long.toString(new Date().getTime());
        session("userTime", tickString);

        return ctx.session().get("userName");
    }

    @Override
    public Result onUnauthorized(Context ctx){
        flash("danger", "You must Login first.");
        return redirect(routes.AdminController.login());
    }
}
