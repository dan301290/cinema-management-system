package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This model handles all Screen related data - providing getters/setters for manipulating Screen data.
 */
@Entity
public class Screen extends Model {
    @Id
    Integer screenId;
    String totalSeats;
    String screenName;

    public static Finder<Integer, Screen> find = new Finder<Integer, Screen>(Screen.class);

    public Screen(){

    }

    public Screen(Integer screenId, String totalSeats, String screenName) {
        this.screenId = screenId;
        this.totalSeats = totalSeats;
        this.screenName = screenName;
    }

    public Integer getScreenId() {
        return screenId;
    }

    public void setScreenId(Integer screenId) {
        this.screenId = screenId;
    }

    public String getTotalSeats() {
        return totalSeats;
    }

    public void setTotalSeats(String totalSeats) {
        this.totalSeats = totalSeats;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
}
