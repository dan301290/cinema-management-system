package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This model handles all movie related data - providing getters/setters for manipulating movie data.
 */

@Entity
public class Movie extends Model{

    @Id
    Integer movieId;
    String movieName;
    String ageRating;
    String shortDesc;
    String plot;
    String img_URL;

    String year;
    String genre;
    String released;
    String director;
    String runtime;

    public static Finder<Integer, Movie> find = new Finder<Integer, Movie>(Movie.class);

    public Movie(){

    }

    public Movie(Integer movieId, String movieName, String ageRating, String shortDesc, String plot, String img_URL
            , String year, String genre, String released, String director, String runtime){
        this.movieId = movieId;
        this.movieName = movieName;
        this.ageRating = ageRating;
        this.shortDesc = shortDesc;
        this.plot = plot;
        this.img_URL = img_URL;
        this.year = year;
        this.genre = genre;
        this.released = released;
        this.director = director;
        this.runtime = runtime;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getAgeRating() {
        return ageRating;
    }

    public void setAgeRating(String ageRating) {
        this.ageRating = ageRating;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getImg_URL() {
        return img_URL;
    }

    public void setImg_URL(String img_URL) {
        this.img_URL = img_URL;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }
}