package models;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints.*;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class User extends Model {
    @Id
    Integer userId;
    @Required
    String userName;
    @Required
    String password;

    public static Finder<Integer, User> find = new Finder<Integer, User>(User.class);

    public User(){

    }

    public User(Integer userId, String userName, String password) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
