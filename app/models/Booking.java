package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This model handles all Booking related data - providing getters/setters for manipulating Booking data.
 */
@Entity
public class Booking extends Model {
    @Id
    Integer bookingId;
    String showingId;
    String customerId;
    String seatNum;
    String date;

    public static Finder<Integer, Booking> find = new Finder<Integer, Booking>(Booking.class);

    public Booking(){

    }

    public Booking(Integer bookingId, String showingId, String customerId, String seatNum, String date) {
        this.bookingId = bookingId;
        this.showingId = showingId;
        this.customerId = customerId;
        this.seatNum = seatNum;
        this.date = date;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public String getShowingId() {
        return showingId;
    }

    public void setShowingId(String showingId) {
        this.showingId = showingId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(String seatNum) {
        this.seatNum = seatNum;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
