package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This model handles all Showing related data - providing getters/setters for manipulating Showing data.
 */
@Entity
public class Showing extends Model {

    @Id
    Integer showingId;
    String screenId;
    String startTime;
    String endTime;
    String date;

    public static Finder<Integer, Showing> find = new Finder<Integer, Showing>(Showing.class);

    public Showing(){

    }

    public Showing(Integer showingId, String screenId, String startTime, String endTime, String date) {
        this.showingId = showingId;
        this.screenId = screenId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.date = date;
    }

    public Integer getShowingId() {
        return showingId;
    }

    public void setShowingId(Integer showingId) {
        this.showingId = showingId;
    }

    public String getScreenId() {
        return screenId;
    }

    public void setScreenId(String screenId) {
        this.screenId = screenId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
