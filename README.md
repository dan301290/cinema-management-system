# Cinema Management System

The Java web application:
  - Runs on Heroku + with a PostGreSQL database.
  - Built on the Java Play Framework + Bootstrap v4 - https://www.playframework.com/
  - Can be accessed here - https://cinema-management-system.herokuapp.com/

# Features
  - Manage Movies, Screens and showing times and dates
  -- Add, Edit and Update Movies. (Movie data pulled from http://www.omdbapi.com/ RESTful api)
  -- Add, Edit and Update Screens. (create screens and seating)
  -- Add, Edit and Update showings. (create showings for movies, provides useful tooltips to prevent creating overlapping showings)
  - Provide cinema usage analytics
  -- Cinema capacity usage
  -- Showings capacity usage
  -- popular showings
  - Add, Edit and Update customers.

### Database - Create Statements

Create Movies table

```sh
$ create table movie (
  movie_id                      integer auto_increment not null,
  movie_name                    varchar(255),
  age_rating                    varchar(255),
  short_desc                    varchar(255),
  plot                          varchar(255),
  img_url                       varchar(255),
  year                          varchar(255),
  genre                         varchar(255),
  released                      varchar(255),
  director                      varchar(255),
  runtime                       varchar(255),
  constraint pk_movie primary key (movie_id));
```

Create Showings table

```sh
$ create table showing (
  showing_id                    integer auto_increment not null,
  screen_id                     varchar(255),
  start_time                    varchar(255),
  end_time                      varchar(255),
  date                          varchar(255),
  constraint pk_showing primary key (showing_id));
```

Create Bookings table

```sh
$ create table booking (
  booking_id                    integer auto_increment not null,
  showing_id                    varchar(255),
  customer_id                   varchar(255),
  seat_num                      varchar(255),
  date                          varchar(255),
  constraint pk_booking primary key (booking_id));
```

Create Customers table

```sh
$ create table customer (
  customer_id                   integer auto_increment not null,
  first_name                    varchar(255),
  last_name                     varchar(255),
  email                         varchar(255),
  phone                         varchar(255),
  address                       varchar(255),
  constraint pk_customer primary key (customer_id));
```

Create Screens table

```sh
$ create table screen (
  screen_id                     integer auto_increment not null,
  total_seats                   varchar(255),
  screen_name                   varchar(255),
  constraint pk_screen primary key (screen_id));
```

Create Users table

```sh
$ create table user (
  user_id                       integer auto_increment not null,
  user_name                     varchar(255),
  password                      varchar(255),
  constraint pk_user primary key (user_id));
```